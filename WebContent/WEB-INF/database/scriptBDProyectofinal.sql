USE [master]
GO
/****** Object:  Database [BDPROYECTOFINAL]    Script Date: 09/04/2018 22:12:12 ******/
CREATE DATABASE [BDPROYECTOFINAL] ON  PRIMARY 
( NAME = N'BDPROYECTOFINAL', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\BDPROYECTOFINAL.mdf' , SIZE = 2304KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BDPROYECTOFINAL_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\BDPROYECTOFINAL_log.LDF' , SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BDPROYECTOFINAL] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BDPROYECTOFINAL].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BDPROYECTOFINAL] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [BDPROYECTOFINAL] SET ANSI_NULLS OFF
GO
ALTER DATABASE [BDPROYECTOFINAL] SET ANSI_PADDING OFF
GO
ALTER DATABASE [BDPROYECTOFINAL] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [BDPROYECTOFINAL] SET ARITHABORT OFF
GO
ALTER DATABASE [BDPROYECTOFINAL] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [BDPROYECTOFINAL] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [BDPROYECTOFINAL] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [BDPROYECTOFINAL] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [BDPROYECTOFINAL] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [BDPROYECTOFINAL] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [BDPROYECTOFINAL] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [BDPROYECTOFINAL] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [BDPROYECTOFINAL] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [BDPROYECTOFINAL] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [BDPROYECTOFINAL] SET  DISABLE_BROKER
GO
ALTER DATABASE [BDPROYECTOFINAL] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [BDPROYECTOFINAL] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [BDPROYECTOFINAL] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [BDPROYECTOFINAL] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [BDPROYECTOFINAL] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [BDPROYECTOFINAL] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [BDPROYECTOFINAL] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [BDPROYECTOFINAL] SET  READ_WRITE
GO
ALTER DATABASE [BDPROYECTOFINAL] SET RECOVERY FULL
GO
ALTER DATABASE [BDPROYECTOFINAL] SET  MULTI_USER
GO
ALTER DATABASE [BDPROYECTOFINAL] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [BDPROYECTOFINAL] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'BDPROYECTOFINAL', N'ON'
GO
USE [BDPROYECTOFINAL]
GO
/****** Object:  Table [dbo].[DISTRIBUIDOR]    Script Date: 09/04/2018 22:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DISTRIBUIDOR](
	[dist_codigo] [char](5) NOT NULL,
	[dist_usuario] [char](200) NOT NULL,
	[dist_password] [varbinary](8000) NOT NULL,
	[dist_estado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[dist_codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UBIGEO]    Script Date: 09/04/2018 22:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UBIGEO](
	[cod_ubigeo] [char](6) NOT NULL,
	[desc_ubigeo] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[cod_ubigeo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ARTICULO]    Script Date: 09/04/2018 22:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ARTICULO](
	[art_codigo] [char](5) NOT NULL,
	[art_descripcion] [varchar](500) NOT NULL,
	[arti_precunidad] [numeric](7, 2) NOT NULL,
	[art_estado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[art_codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CLIENTE]    Script Date: 09/04/2018 22:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CLIENTE](
	[cli_codigo] [char](5) NOT NULL,
	[cli_apellido] [varchar](200) NOT NULL,
	[cli_nombre] [varchar](200) NOT NULL,
	[cli_movil1] [char](9) NOT NULL,
	[cli_movil2] [char](9) NOT NULL,
	[cli_email] [varchar](150) NOT NULL,
	[cli_direc] [varchar](500) NOT NULL,
	[cli_ubigeo] [char](6) NOT NULL,
	[cli_imagen] [varchar](max) NULL,
	[cli_latitud] [varchar](max) NOT NULL,
	[cli_longitud] [varchar](max) NOT NULL,
	[cli_estado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[cli_codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[REPARTO]    Script Date: 09/04/2018 22:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[REPARTO](
	[rep_codigo] [char](5) NOT NULL,
	[rep_fecemi] [varchar](45) NOT NULL,
	[rep_observ] [text] NOT NULL,
	[dist_codigo] [char](5) NOT NULL,
	[rep_estado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[rep_codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PEDIDO]    Script Date: 09/04/2018 22:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PEDIDO](
	[ped_codigo] [char](5) NOT NULL,
	[ped_fecemi] [varchar](45) NOT NULL,
	[ped_clicod] [char](5) NOT NULL,
	[ped_estado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ped_codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DETALLEREPARTOXPEDIDO]    Script Date: 09/04/2018 22:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DETALLEREPARTOXPEDIDO](
	[repd_repcod] [char](5) NOT NULL,
	[repd_pedcod] [char](5) NOT NULL,
	[repd_estado] [varchar](45) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DETALLEPEDIDOXARTICULO]    Script Date: 09/04/2018 22:12:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DETALLEPEDIDOXARTICULO](
	[pedd_codigo] [char](5) NOT NULL,
	[pedd_artcod] [char](5) NOT NULL,
	[pedd_cantidad] [int] NOT NULL,
	[pedd_precio] [numeric](7, 2) NOT NULL,
	[pedd_estado] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[SELPEDIDO]    Script Date: 09/04/2018 22:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----SELECT--------------
CREATE procedure [dbo].[SELPEDIDO]--Nombre Procedimiento
as
SET NOCOUNT ON;  
    SELECT * FROM PEDIDO a
GO
/****** Object:  StoredProcedure [dbo].[UPDPEDIDO]    Script Date: 09/04/2018 22:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-------------------------ACTUALIZAR
CREATE PROCEDURE [dbo].[UPDPEDIDO]
    @ped_fecemi AS VARCHAR(45),
    @ped_clicod AS CHAR(5),
    @ped_estado INT,
    @codigo AS CHAR(5)

AS
BEGIN

UPDATE PEDIDO set ped_fecemi=@ped_fecemi, ped_clicod=@ped_clicod, ped_estado=@ped_estado where ped_codigo= @codigo 

END
GO
/****** Object:  StoredProcedure [dbo].[INSPEDIDO]    Script Date: 09/04/2018 22:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---

-----------------INSERT----------------------
CREATE PROCEDURE [dbo].[INSPEDIDO]

    @ped_clicod AS CHAR(5),
    @ped_estado INT
AS
BEGIN

   
	declare @codigo AS CHAR(5) =(select 'P' + right('000' + ltrim(right(isnull(max(ped_codigo),'P0000'),4) +1),4) from PEDIDO)
	declare @ped_fecemi AS VARCHAR(45) = (select CONVERT(VARCHAR(10), GETDATE(), 103) + ' '  + convert(VARCHAR(8), GETDATE(), 14))

        INSERT INTO PEDIDO(ped_codigo,ped_fecemi, ped_clicod, ped_estado) VALUES (@codigo, @ped_fecemi, @ped_clicod, @ped_estado)

END
GO
/****** Object:  StoredProcedure [dbo].[SELPEDIDOXCOD]    Script Date: 09/04/2018 22:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---

-----------------------SELECT X COD-------------
create procedure [dbo].[SELPEDIDOXCOD]--Nombre Procedimiento
@codigo char(5)
as
SET NOCOUNT ON;  
    SELECT * FROM PEDIDO a WHERE a.ped_codigo=@codigo
GO
/****** Object:  StoredProcedure [dbo].[UPDREPARTODETALLE]    Script Date: 09/04/2018 22:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-------------------------

-------------------------ACTUALIZAR
CREATE PROCEDURE [dbo].[UPDREPARTODETALLE]
   @rep_fecemi AS VARCHAR(45),
    @rep_observ AS TEXT,
	@dist_codigo AS CHAR(5),
    @rep_estado INT,
	@repd_pedcod AS CHAR(5),
	@repd_estado INT,
	@codigo AS CHAR(5)

AS
BEGIN

		 UPDATE DETALLEREPARTOXPEDIDO  SET repd_pedcod= @repd_pedcod,repd_estado=@repd_estado where  repd_repcod=@codigo
		
        UPDATE REPARTO SET rep_fecemi=@rep_fecemi, rep_observ=@rep_observ, dist_codigo=@dist_codigo,rep_estado=@rep_estado where rep_codigo= @codigo
      
        
END
GO
/****** Object:  StoredProcedure [dbo].[UPDPEDIDODETALLE]    Script Date: 09/04/2018 22:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-------------------------ACTUALIZAR
CREATE PROCEDURE [dbo].[UPDPEDIDODETALLE]
	@pedd_artcod AS CHAR(5),
	@pedd_cantidad INT,
	@pedd_precio AS numeric(7,2),
	@pedd_estado INT,
	@codigo AS CHAR(5)

AS
BEGIN
UPDATE  DETALLEPEDIDOXARTICULO set  pedd_artcod=@pedd_artcod,pedd_cantidad=@pedd_cantidad,pedd_precio=@pedd_precio,pedd_estado=@pedd_estado where pedd_codigo=@codigo    
END
GO
/****** Object:  StoredProcedure [dbo].[SELPEDIDODETALLEXCODPEDIDO]    Script Date: 09/04/2018 22:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----SELECT--------------
CREATE procedure [dbo].[SELPEDIDODETALLEXCODPEDIDO]--Nombre Procedimiento
@CODPED AS CHAR(5)
as
SET NOCOUNT ON;  
    SELECT b.* FROM DETALLEPEDIDOXARTICULO b, PEDIDO p WHERE p.ped_codigo = @CODPED and p.ped_codigo = b.pedd_codigo
GO
/****** Object:  StoredProcedure [dbo].[SELPEDIDODETALLE]    Script Date: 09/04/2018 22:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----SELECT--------------
create procedure [dbo].[SELPEDIDODETALLE]--Nombre Procedimiento
as
SET NOCOUNT ON;  
    SELECT * FROM DETALLEPEDIDOXARTICULO
GO
/****** Object:  StoredProcedure [dbo].[SELRUTADETALLE]    Script Date: 09/04/2018 22:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SELRUTADETALLE]
AS
SELECT p.ped_codigo as rt_codigo,p.ped_clicod as rt_clicod,cli.cli_latitud as rt_latitud,cli.cli_longitud as rt_longitud,cli.cli_direc as rt_direc,cli.cli_movil1 as rt_movil1,cli.cli_movil2 as rt_movil2 
FROM PEDIDO p,CLIENTE cli,REPARTO r,DETALLEREPARTOXPEDIDO d
WHERE cli.cli_codigo = p.ped_clicod AND d.repd_pedcod = p.ped_codigo and d.repd_repcod = r.rep_codigo and r.rep_fecemi = (SELECT CONVERT(VARCHAR(10), GETDATE(), 103))
GO
/****** Object:  StoredProcedure [dbo].[SELREPARTODETALLEXCOD]    Script Date: 09/04/2018 22:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-----------------------SELECT X COD-------------
create procedure [dbo].[SELREPARTODETALLEXCOD]--Nombre Procedimiento
@codigo char(5)
as
SET NOCOUNT ON;  
   SELECT a.rep_codigo,a.rep_fecemi,a.rep_observ,a.dist_codigo,a.rep_estado,b.repd_repcod,b.repd_pedcod,b.repd_estado 
    FROM REPARTO a, DETALLEREPARTOXPEDIDO b
    WHERE a.rep_codigo = b.repd_repcod
	and a.rep_codigo=@codigo
GO
/****** Object:  StoredProcedure [dbo].[SELREPARTODETALLE]    Script Date: 09/04/2018 22:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----SELECT--------------
CREATE procedure [dbo].[SELREPARTODETALLE]--Nombre Procedimiento
as
SET NOCOUNT ON;  
  SELECT a.rep_codigo,a.rep_fecemi,a.rep_observ,a.dist_codigo,a.rep_estado,b.repd_repcod,b.repd_pedcod,b.repd_estado 
    FROM REPARTO a, DETALLEREPARTOXPEDIDO b
    WHERE a.rep_codigo = b.repd_repcod and a.rep_fecemi = (SELECT CONVERT(VARCHAR(10), GETDATE(), 103))
GO
/****** Object:  StoredProcedure [dbo].[SELDATECOUNTREPARTO]    Script Date: 09/04/2018 22:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SELDATECOUNTREPARTO]
AS

SELECT DISTINCT(SELECT DISTINCT r.rep_fecemi from REPARTO r) AS RUTA_FECHA,(SELECT COUNT(DISTINCT p.ped_clicod) from PEDIDO p) AS RUTA_NROCLIENTES from 
REPARTO r, PEDIDO p,DETALLEREPARTOXPEDIDO d WHERE
r.rep_codigo = d.repd_repcod AND d.repd_pedcod = p.ped_codigo and r.rep_fecemi = (SELECT CONVERT(VARCHAR(10), GETDATE(), 103)) GROUP BY p.ped_clicod,r.rep_fecemi
GO
/****** Object:  StoredProcedure [dbo].[SELARTICULOSXRUTA]    Script Date: 09/04/2018 22:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SELARTICULOSXRUTA]
@IDCLIENTE CHAR(5)
AS
SELECT a.art_codigo as ART_CODIGO,a.art_descripcion as ART_DESCRIPCION,A.arti_precunidad as ART_PRECUNIDAD,d.pedd_cantidad as ART_CANTIDAD,D.pedd_precio as ART_PRECIOTOTAL FROM ARTICULO a, DETALLEPEDIDOXARTICULO d,PEDIDO p,CLIENTE c 
Where d.pedd_artcod = a.art_codigo AND p.ped_codigo = d.pedd_codigo AND p.ped_clicod = @IDCLIENTE
AND c.cli_codigo = @IDCLIENTE
GO
/****** Object:  StoredProcedure [dbo].[INSREPARTODETALLE]    Script Date: 09/04/2018 22:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---

-----------------INSERT----------------------
CREATE PROCEDURE [dbo].[INSREPARTODETALLE]

    @rep_fecemi AS VARCHAR(45),
    @rep_observ AS TEXT,
	@dist_codigo AS CHAR(5),
    @rep_estado INT,
	@repd_pedcod AS CHAR(5),
	@repd_estado INT

AS
BEGIN

   
	declare @codigo AS CHAR(5) =(select 'R' + right('000' + ltrim(right(isnull(max(rep_codigo),'R0000'),4) +1),4) from REPARTO)

        INSERT INTO REPARTO(rep_codigo,rep_fecemi, rep_observ, dist_codigo,rep_estado) VALUES (@codigo, @rep_fecemi, @rep_observ,@dist_codigo, @rep_estado)

        INSERT INTO DETALLEREPARTOXPEDIDO(repd_repcod, repd_pedcod,repd_estado) VALUES (@codigo, @repd_pedcod,@repd_estado)

END
GO
/****** Object:  StoredProcedure [dbo].[INSPEDIDODETALLE]    Script Date: 09/04/2018 22:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---

-----------------INSERT----------------------
CREATE PROCEDURE [dbo].[INSPEDIDODETALLE]
	@pedd_artcod AS CHAR(5),
	@pedd_cantidad INT,
	@pedd_precio AS numeric(7,2),
	@pedd_estado INT
AS   
    BEGIN 
    
    declare @codigo AS CHAR(5) =(SELECT isnull(max(ped_codigo),'P0001') from PEDIDO)
 INSERT INTO DETALLEPEDIDOXARTICULO(pedd_codigo, pedd_artcod,pedd_cantidad,pedd_precio,pedd_estado) VALUES (@codigo, @pedd_artcod,@pedd_cantidad,@pedd_precio,@pedd_estado)
END
GO
/****** Object:  StoredProcedure [dbo].[DELETEREPARTODETALLE]    Script Date: 09/04/2018 22:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------

------------------ELIMINAR
CREATE procedure [dbo].[DELETEREPARTODETALLE]
(
@codigo char(5)
) 
as
delete from DETALLEREPARTOXPEDIDO where repd_repcod=@codigo
delete from REPARTO where rep_codigo= @codigo
GO
/****** Object:  StoredProcedure [dbo].[DELETEPEDIDODETALLE]    Script Date: 09/04/2018 22:12:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[DELETEPEDIDODETALLE]
(
@codigo char(5)
) 
as
delete from DETALLEPEDIDOXARTICULO where pedd_codigo=@codigo
delete from PEDIDO where ped_codigo= @codigo
GO
/****** Object:  ForeignKey [FK__CLIENTE__cli_ubi__08EA5793]    Script Date: 09/04/2018 22:12:14 ******/
ALTER TABLE [dbo].[CLIENTE]  WITH CHECK ADD FOREIGN KEY([cli_ubigeo])
REFERENCES [dbo].[UBIGEO] ([cod_ubigeo])
GO
/****** Object:  ForeignKey [FK__REPARTO__dist_co__1920BF5C]    Script Date: 09/04/2018 22:12:14 ******/
ALTER TABLE [dbo].[REPARTO]  WITH CHECK ADD FOREIGN KEY([dist_codigo])
REFERENCES [dbo].[DISTRIBUIDOR] ([dist_codigo])
GO
/****** Object:  ForeignKey [FK__PEDIDO__ped_clic__0DAF0CB0]    Script Date: 09/04/2018 22:12:14 ******/
ALTER TABLE [dbo].[PEDIDO]  WITH CHECK ADD FOREIGN KEY([ped_clicod])
REFERENCES [dbo].[CLIENTE] ([cli_codigo])
GO
/****** Object:  ForeignKey [FK__DETALLERE__repd___1B0907CE]    Script Date: 09/04/2018 22:12:14 ******/
ALTER TABLE [dbo].[DETALLEREPARTOXPEDIDO]  WITH CHECK ADD FOREIGN KEY([repd_repcod])
REFERENCES [dbo].[REPARTO] ([rep_codigo])
GO
/****** Object:  ForeignKey [FK__DETALLERE__repd___1BFD2C07]    Script Date: 09/04/2018 22:12:14 ******/
ALTER TABLE [dbo].[DETALLEREPARTOXPEDIDO]  WITH CHECK ADD FOREIGN KEY([repd_pedcod])
REFERENCES [dbo].[PEDIDO] ([ped_codigo])
GO
/****** Object:  ForeignKey [FK__DETALLEPE__pedd___0F975522]    Script Date: 09/04/2018 22:12:14 ******/
ALTER TABLE [dbo].[DETALLEPEDIDOXARTICULO]  WITH CHECK ADD FOREIGN KEY([pedd_codigo])
REFERENCES [dbo].[PEDIDO] ([ped_codigo])
GO
/****** Object:  ForeignKey [FK__DETALLEPE__pedd___108B795B]    Script Date: 09/04/2018 22:12:14 ******/
ALTER TABLE [dbo].[DETALLEPEDIDOXARTICULO]  WITH CHECK ADD FOREIGN KEY([pedd_artcod])
REFERENCES [dbo].[ARTICULO] ([art_codigo])
GO
