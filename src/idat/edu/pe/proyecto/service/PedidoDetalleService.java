package idat.edu.pe.proyecto.service;

import java.util.List;

import idat.edu.pe.proyecto.bean.PedidoDetalleBean;

public interface PedidoDetalleService {
	public List<PedidoDetalleBean> selPedidosDetalle() throws Exception;
	public List<PedidoDetalleBean> selPedidosDetallexCod(String codped) throws Exception;
	public boolean insPedidoDetalle(PedidoDetalleBean newPedido) throws Exception;
}
