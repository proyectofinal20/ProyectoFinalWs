package idat.edu.pe.proyecto.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import idat.edu.pe.proyecto.bean.RepartoBean;

import idat.edu.pe.proyecto.dao.RepartoDAOImpl;

@Service("RepartoService")
public class RepartoServiceImpl implements RepartoService {
private static final Log log = LogFactory.getLog(RepartoServiceImpl.class);
	
	
	@Autowired
	private RepartoDAOImpl repartoDAO;
	
	

	@Override
	public List<RepartoBean> listarRepartos() throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - RepartoServiceImpl.listarRepartos");
		List<RepartoBean> lst = new ArrayList<RepartoBean>();
		try {
			lst =repartoDAO.selRepartos();		
			for(int x=0; x < lst.size(); x++) {
				RepartoBean item = lst.get(x);
				item.setRepd_desestado(item.getRepd_estado() == 0 ? "INACTIVO" : "ACTIVO");
				
				
			}
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - Inicio -RepartoServiceImpl.listarRepartos");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - Inicio - RepartoServiceImpl.listarRepartos");
		} 	
		
		return lst;
	}

	@Override
	public RepartoBean listarReparto(String rep_codigo) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - RepartoServiceImpl.listarReparto");
		RepartoBean reparto = null;	
		try {
			reparto = (RepartoBean) repartoDAO.selReparto(rep_codigo);		
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - RepartoServiceImpl.listarReparto");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - RepartoServiceImpl.listarReparto");
		} 	
		
		return reparto;
	}

	@Override
	public boolean insertarReparto(RepartoBean c) throws Exception {
		boolean result;
		try {
			result = repartoDAO.insReparto(c);
		} catch (Exception e) {
			if (log.isDebugEnabled()) log.debug("Error - RepartoServiceImpl.insertarReparto");
		    log.error(e, e);
		    throw e;
		}finally {
			if(log.isDebugEnabled()) log.debug("Final - RepartoServiceImpl.insertarReparto");
		}
		return result;
	}

	@Override
	public boolean actualizarReparto(RepartoBean c) throws Exception {
		boolean result;
		try {
			result = repartoDAO.updReparto(c);
		} catch (Exception e) {
			if (log.isDebugEnabled()) log.debug("Error - RepartoServiceImpl.actualizarReparto");
		    log.error(e, e);
		    throw e;
		}finally {
			if(log.isDebugEnabled()) log.debug("Final - RepartoServiceImpl.actualizarReparto");
		}
		return result;
	}

	@Override
	public boolean eliminarReparto(String rep_codigo) throws Exception {
		boolean result;
		try {
			result = repartoDAO.delReparto(rep_codigo);
		} catch (Exception e) {
			if (log.isDebugEnabled()) log.debug("Error - RepartoServiceImpl.eliminarReparto");
		    log.error(e, e);
		    throw e;
		}finally {
			if(log.isDebugEnabled()) log.debug("Final - RepartoServiceImpl.eliminarReparto");
		}
		return result;
	}

	@Override
	public List<RepartoBean> listarRutas() throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - RepartoServiceImpl.listarRepartos");
		List<RepartoBean> lst = new ArrayList<RepartoBean>();
		try {
			lst =repartoDAO.selRutas();		
				
			
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - Inicio -RepartoServiceImpl.listarRepartos");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - Inicio - RepartoServiceImpl.listarRepartos");
		} 	
		
		return lst;
	}

}
