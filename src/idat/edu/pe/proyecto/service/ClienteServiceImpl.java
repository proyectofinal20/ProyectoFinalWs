package idat.edu.pe.proyecto.service;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import idat.edu.pe.proyecto.bean.ClienteBean;
import idat.edu.pe.proyecto.dao.ClienteDAOImpl;

@Service("ClienteService")
public class ClienteServiceImpl implements ClienteService {
	private static final Log log = LogFactory.getLog(ClienteServiceImpl.class);
	
	
	@Autowired
	private ClienteDAOImpl clienteDAO;
	
	
	
	
	@Override
	public List<ClienteBean> listarClientes() throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - ClienteServiceImpl.listarClientes");
		List<ClienteBean> lst = new ArrayList<ClienteBean>();
		try {
			lst =clienteDAO.selClientes();		
			for(int x=0; x < lst.size(); x++) {
				ClienteBean item = lst.get(x);
				item.setCli_desestado(item.getCli_estado() == 0 ? "INACTIVO" : "ACTIVO");
				
				
			}
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - Inicio -ClienteServiceImpl.listarClientes");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - Inicio - ClienteServiceImpl.listarClientes");
		} 	
		
		return lst;
		
	}

	@Override
	public ClienteBean listarCliente(String cli_codigo) throws Exception {
		
		if (log.isDebugEnabled()) log.debug("Inicio - ClienteServiceImpl.listarCliente");
		ClienteBean cliente = null;	
		try {
			cliente = (ClienteBean) clienteDAO.selCliente(cli_codigo);		
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - ClienteServiceImpl.listarCliente");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - ClienteServiceImpl.listarCliente");
		} 	
		
		return cliente;
	}
		
		
	

	@Override
	public boolean insertarCliente(ClienteBean c) throws Exception {
		boolean result;
		try {
			result = clienteDAO.insCliente(c);
		} catch (Exception e) {
			if (log.isDebugEnabled()) log.debug("Error - ClienteServiceImpl.insertarCliente");
		    log.error(e, e);
		    throw e;
		}finally {
			if(log.isDebugEnabled()) log.debug("Final - ClienteServiceImpl.insertarCliente");
		}
		return result;
		
	}

	@Override
	public boolean actualizarCliente(ClienteBean c) throws Exception {
		boolean result;
		try {
			result = clienteDAO.updCliente(c);
		} catch (Exception e) {
			if (log.isDebugEnabled()) log.debug("Error - ClienteServiceImpl.actualizarCliente");
		    log.error(e, e);
		    throw e;
		}finally {
			if(log.isDebugEnabled()) log.debug("Final - ClienteServiceImpl.actualizarCliente");
		}
		return result;
	}

	@Override
	public boolean eliminarCliente(String cli_codigo) throws Exception {
		boolean result;
		try {
			result = clienteDAO.delCliente(cli_codigo);
		} catch (Exception e) {
			if (log.isDebugEnabled()) log.debug("Error - ClienteServiceImpl.eliminarCliente");
		    log.error(e, e);
		    throw e;
		}finally {
			if(log.isDebugEnabled()) log.debug("Final - ClienteServiceImpl.eliminarCliente");
		}
		return result;
	}


}
