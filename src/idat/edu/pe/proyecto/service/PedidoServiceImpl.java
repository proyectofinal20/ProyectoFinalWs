package idat.edu.pe.proyecto.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import idat.edu.pe.proyecto.bean.PedidoBean;

import idat.edu.pe.proyecto.dao.PedidoDAOImpl;

@Service("PedidoService")
public class PedidoServiceImpl implements PedidoService {
	private static final Log log = LogFactory.getLog(PedidoServiceImpl.class);
	
	
	@Autowired
	private PedidoDAOImpl pedidoDAO;
	
	
	
	@Override
	public List<PedidoBean> listarPedidos() throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - PedidoServiceImpl.listarPedidos");
		List<PedidoBean> lst = new ArrayList<PedidoBean>();
		try {
			lst =pedidoDAO.selPedidos();		
			for(int x=0; x < lst.size(); x++) {
				PedidoBean item = lst.get(x);
				item.setPedd_desestado(item.getPed_estado() == 0 ? "INACTIVO" : "ACTIVO");
				
				
			}
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - Inicio -PedidoServiceImpl.listarPedidos");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - Inicio - PedidoServiceImpl.listarPedidos");
		} 	
		
		return lst;
	}

	@Override
	public PedidoBean listarPedido(String ped_codigo) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - PedidoServiceImpl.listarPedido");
		PedidoBean pedido = null;	
		try {
			pedido = (PedidoBean) pedidoDAO.selPedido(ped_codigo);		
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - PedidoServiceImpl.listarPedido");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - PedidoServiceImpl.listarPedido");
		} 	
		
		return pedido;
	}

	@Override
	public boolean insertarPedido(PedidoBean c) throws Exception {
		boolean result;
		try {
			result = pedidoDAO.insPedido(c);
		} catch (Exception e) {
			if (log.isDebugEnabled()) log.debug("Error - PedidoServiceImpl.insertarPedido");
		    log.error(e, e);
		    throw e;
		}finally {
			if(log.isDebugEnabled()) log.debug("Final - PedidoServiceImpl.insertarPedido");
		}
		return result;
		
	}

	@Override
	public boolean actualizarPedido(PedidoBean c) throws Exception {
		boolean result;
		try {
			result = pedidoDAO.updPedido(c);
		} catch (Exception e) {
			if (log.isDebugEnabled()) log.debug("Error - PedidoServiceImpl.actualizarPedido");
		    log.error(e, e);
		    throw e;
		}finally {
			if(log.isDebugEnabled()) log.debug("Final - PedidoServiceImpl.actualizarPedido");
		}
		return result;
	}

	@Override
	public boolean eliminarPedido(String ped_codigo) throws Exception {
		boolean result;
		try {
			result = pedidoDAO.delPedido(ped_codigo);
		} catch (Exception e) {
			if (log.isDebugEnabled()) log.debug("Error - PedidoServiceImpl.eliminarPedido");
		    log.error(e, e);
		    throw e;
		}finally {
			if(log.isDebugEnabled()) log.debug("Final - PedidoServiceImpl.eliminarPedido");
		}
		return result;
	}

}
