package idat.edu.pe.proyecto.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import idat.edu.pe.proyecto.bean.ArticuloBean;
import idat.edu.pe.proyecto.dao.ArticuloDAOImpl;

@Service("ArticuloService")
public class ArticuloServiceImpl implements ArticuloService {
	private static final Log log = LogFactory.getLog(ArticuloServiceImpl.class);
	
	@Autowired
	private ArticuloDAOImpl articuloDAO;
	
	@Override
	public List<ArticuloBean> selArticulos() throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - ArticuloServiceImpl.listarArticulos");
		List<ArticuloBean> lst = new ArrayList<ArticuloBean>();
		try {
			lst =articuloDAO.selArticulos();		
			for(int x=0; x < lst.size(); x++) {
				ArticuloBean item = lst.get(x);
				item.setArt_desestado(item.getArt_estado() == 0 ? "INACTIVO" : "ACTIVO");
				
				
			}
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - Inicio -ArticuloServiceImpl.listarArticulos");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - Inicio - ArticuloServiceImpl.listarArticulos");
		} 	
		
		return lst;
	}

	@Override
	public ArticuloBean selArticulo(String art_codigo) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - ArticuloServiceImpl.listarArticulo");
		ArticuloBean articulo = null;	
		try {
			articulo = (ArticuloBean) articuloDAO.selArticulo(art_codigo);		
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - ArticuloServiceImpl.listarArticulo");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - ArticuloServiceImpl.listarArticulo");
		} 	
		
		return articulo;
	}

	@Override
	public boolean insArticulo(ArticuloBean newArticulo) throws Exception {
		boolean result;
		try {
			result = articuloDAO.insArticulo(newArticulo);
		} catch (Exception e) {
			if (log.isDebugEnabled()) log.debug("Error - ArticuloServiceImpl.insertarArticulo");
		    log.error(e, e);
		    throw e;
		}finally {
			if(log.isDebugEnabled()) log.debug("Final - ArticuloServiceImpl.insertarArticulo");
		}
		return result;
	}

	@Override
	public boolean updArticulo(ArticuloBean updArticulo) throws Exception {
		boolean result;
		try {
			result = articuloDAO.updArticulo(updArticulo);
		} catch (Exception e) {
			if (log.isDebugEnabled()) log.debug("Error - ArticuloServiceImpl.actualizarArticulo");
		    log.error(e, e);
		    throw e;
		}finally {
			if(log.isDebugEnabled()) log.debug("Final - ArticuloServiceImpl.actualizarArticulo");
		}
		return result;
	}

	@Override
	public boolean delArticulo(String art_codigo) throws Exception {
		boolean result;
		try {
			result = articuloDAO.delArticulo(art_codigo);
		} catch (Exception e) {
			if (log.isDebugEnabled()) log.debug("Error - ArticuloServiceImpl.eliminarArticulo");
		    log.error(e, e);
		    throw e;
		}finally {
			if(log.isDebugEnabled()) log.debug("Final - ArticuloServiceImpl.eliminarArticulo");
		}
		return result;
	}

}
