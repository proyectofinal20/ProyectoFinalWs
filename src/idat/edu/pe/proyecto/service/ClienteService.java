package idat.edu.pe.proyecto.service;
import java.util.List;
import idat.edu.pe.proyecto.bean.ClienteBean;

public interface ClienteService {

	public List<ClienteBean> listarClientes() throws Exception;
	public ClienteBean listarCliente(String cli_codigo) throws Exception;
    public boolean insertarCliente(ClienteBean c) throws Exception;
    public boolean actualizarCliente(ClienteBean c) throws Exception;
    public boolean eliminarCliente(String cli_codigo) throws Exception;

}
