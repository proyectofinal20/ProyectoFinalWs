package idat.edu.pe.proyecto.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import idat.edu.pe.proyecto.bean.PedidoDetalleBean;
import idat.edu.pe.proyecto.dao.PedidoDetalleDAOImpl;

@Service("PedidoDetalleService")
public class PedidoDetalleServiceImpl implements PedidoDetalleService{

private static final Log log = LogFactory.getLog(PedidoServiceImpl.class);
	
	
	@Autowired
	private PedidoDetalleDAOImpl pedidodetalleDAO;
	
	@Override
	public List<PedidoDetalleBean> selPedidosDetalle() throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - PedidoDetalleServiceImpl.listarPedidos");
		List<PedidoDetalleBean> lst = new ArrayList<PedidoDetalleBean>();
		try {
			lst =pedidodetalleDAO.selPedidosDetalle();		
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - Inicio -PedidoServiceImpl.listarPedidos");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - Inicio - PedidoServiceImpl.listarPedidos");
		} 	
		
		return lst;
	}
	
	@Override
	public List<PedidoDetalleBean> selPedidosDetallexCod(String codped) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - PedidoDetalleServiceImpl.listarPedidos");
		List<PedidoDetalleBean> lst = new ArrayList<PedidoDetalleBean>();
		try {
			lst =pedidodetalleDAO.selPedidosDetallexCod(codped);		
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - Inicio -PedidoServiceImpl.listarPedidos");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - Inicio - PedidoServiceImpl.listarPedidos");
		} 	
		
		return lst;
	}

	@Override
	public boolean insPedidoDetalle(PedidoDetalleBean c) throws Exception {
		boolean result;
		try {
			result = pedidodetalleDAO.insPedidoDetalle(c);
		} catch (Exception e) {
			if (log.isDebugEnabled()) log.debug("Error - PedidoServiceImpl.insertarPedido");
		    log.error(e, e);
		    throw e;
		}finally {
			if(log.isDebugEnabled()) log.debug("Final - PedidoServiceImpl.insertarPedido");
		}
		return result;
	}

}
