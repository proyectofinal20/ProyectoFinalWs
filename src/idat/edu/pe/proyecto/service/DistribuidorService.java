package idat.edu.pe.proyecto.service;
import java.util.List;

import idat.edu.pe.proyecto.bean.DistribuidorBean;

public interface DistribuidorService {
	
	public List<DistribuidorBean> listarDistribuidors() throws Exception;
	public DistribuidorBean listarDistribuidor(String dist_codigo) throws Exception;
	public DistribuidorBean loginDistribuidor(String dist_usuario,String dist_password) throws Exception;
    public boolean insertarDistribuidor(DistribuidorBean c) throws Exception;
    public boolean actualizarDistribuidor(DistribuidorBean c) throws Exception;
    public boolean eliminarDistribuidor(String dist_codigo) throws Exception;

}
