package idat.edu.pe.proyecto.service;

import java.util.List;

import idat.edu.pe.proyecto.bean.RutaBean;

public interface RutaService {
	public List<RutaBean> selRutas() throws Exception;
}
