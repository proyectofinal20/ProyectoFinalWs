package idat.edu.pe.proyecto.service;

import java.util.List;

import idat.edu.pe.proyecto.bean.RepartoBean;

public interface RepartoService {
	
	
	public List<RepartoBean> listarRepartos() throws Exception;
	public List<RepartoBean> listarRutas() throws Exception;
	public RepartoBean listarReparto(String rep_codigo) throws Exception;
    public boolean insertarReparto(RepartoBean c) throws Exception;
    public boolean actualizarReparto(RepartoBean c) throws Exception;
    public boolean eliminarReparto(String rep_codigo) throws Exception;

}
