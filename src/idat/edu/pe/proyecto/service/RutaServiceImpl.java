package idat.edu.pe.proyecto.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import idat.edu.pe.proyecto.bean.RutaBean;
import idat.edu.pe.proyecto.dao.RutaDAOImpl;

@Service("RutaService")
public class RutaServiceImpl implements RutaService{

	private static final Log log = LogFactory.getLog(RutaServiceImpl.class);
	
	
	@Autowired
	private RutaDAOImpl rutaDAO;
	
	@Override
	public List<RutaBean> selRutas() throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - RutaServiceImpl.listarRutas");
		List<RutaBean> lst = new ArrayList<RutaBean>();
		try {
			lst =rutaDAO.selRutas();		
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - Inicio -PedidoServiceImpl.listarRutas");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - Inicio - PedidoServiceImpl.listarRutas");
		} 	
		
		return lst;
	}

}
