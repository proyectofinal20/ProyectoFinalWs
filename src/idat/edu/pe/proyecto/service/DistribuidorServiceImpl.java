package idat.edu.pe.proyecto.service;

import java.util.ArrayList;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import idat.edu.pe.proyecto.bean.DistribuidorBean;

import idat.edu.pe.proyecto.dao.DistribuidorDAOImpl;


@Service("DistribuidorService")
public class DistribuidorServiceImpl  implements DistribuidorService{
	
private static final Log log = LogFactory.getLog(DistribuidorServiceImpl.class);
	
	
	@Autowired
	private DistribuidorDAOImpl distribuidorDAO;
	
	
	

	@Override
	public List<DistribuidorBean> listarDistribuidors() throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - DistribuidorServiceImpl.listarDistribuidors");
		List<DistribuidorBean> lst = new ArrayList<DistribuidorBean>();
		try {
			lst =distribuidorDAO.selDistribuidors();		
			for(int x=0; x < lst.size(); x++) {
				DistribuidorBean item = lst.get(x);
				item.setDist_desestado(item.getDist_estado() == 0 ? "INACTIVO" : "ACTIVO");
			}
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - Inicio -DistribuidorServiceImpl.listarDistribuidors");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - Inicio - DistribuidorServiceImpl.listarDistribuidors");
		} 	
		
		return lst;
	}

	@Override
	public DistribuidorBean listarDistribuidor(String dist_codigo) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - DistribuidorServiceImpl.listarDistribuidor");
		DistribuidorBean distribuidor = null;	
		try {
			distribuidor = (DistribuidorBean) distribuidorDAO.selDistribuidor(dist_codigo);		
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - DistribuidorServiceImpl.listarDistribuidor");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - DistribuidorServiceImpl.listarDistribuidor");
		} 	
		
		return distribuidor;
	}
	

	@Override
	public DistribuidorBean loginDistribuidor(String dist_usuario, String dist_password) throws Exception {
		

		if (log.isDebugEnabled()) log.debug("Inicio - DistribuidorServiceImpl.listarDistribuidor");
		DistribuidorBean distribuidor = null;	
		try {
			distribuidor = (DistribuidorBean) distribuidorDAO.logDistribuidor(dist_usuario,dist_password);		
		}catch(Exception ex) {
			if (log.isDebugEnabled()) log.debug("Error - DistribuidorServiceImpl.listarDistribuidor");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - DistribuidorServiceImpl.listarDistribuidor");
		} 	
		
		return distribuidor;
	}

	@Override
	public boolean insertarDistribuidor(DistribuidorBean c) throws Exception {
		boolean result;
		try {
			result = distribuidorDAO.insDistribuidor(c);
		} catch (Exception e) {
			if (log.isDebugEnabled()) log.debug("Error - DistribuidorServiceImpl.insertarDistribuidor");
		    log.error(e, e);
		    throw e;
		}finally {
			if(log.isDebugEnabled()) log.debug("Final - DistribuidorServiceImpl.insertarDistribuidor");
		}
		return result;
		
	}

	@Override
	public boolean actualizarDistribuidor(DistribuidorBean c) throws Exception {
		boolean result;
		try {
			result = distribuidorDAO.updDistribuidor(c);
		} catch (Exception e) {
			if (log.isDebugEnabled()) log.debug("Error - DistribuidorServiceImpl.actualizarDistribuidor");
		    log.error(e, e);
		    throw e;
		}finally {
			if(log.isDebugEnabled()) log.debug("Final - DistribuidorServiceImpl.actualizarDistribuidor");
		}
		return result;
	}

	@Override
	public boolean eliminarDistribuidor(String dist_codigo) throws Exception {
		boolean result;
		try {
			result = distribuidorDAO.delDistribuidor(dist_codigo);
		} catch (Exception e) {
			if (log.isDebugEnabled()) log.debug("Error - DistribuidorServiceImpl.eliminarDistribuidor");
		    log.error(e, e);
		    throw e;
		}finally {
			if(log.isDebugEnabled()) log.debug("Final - DistribuidorServiceImpl.eliminarDistribuidor");
		}
		return result;
	}

}
