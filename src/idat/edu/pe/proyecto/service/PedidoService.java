package idat.edu.pe.proyecto.service;

import java.util.List;

import idat.edu.pe.proyecto.bean.PedidoBean;

public interface PedidoService {

	public List<PedidoBean> listarPedidos() throws Exception;
	public PedidoBean listarPedido(String ped_codigo) throws Exception;
    public boolean insertarPedido(PedidoBean c) throws Exception;
    public boolean actualizarPedido(PedidoBean c) throws Exception;
    public boolean eliminarPedido(String ped_codigo) throws Exception;
	
}
