package idat.edu.pe.proyecto.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import idat.edu.pe.proyecto.bean.PedidoBean;



public class PedidoDAOImpl implements PedidoDAO {
	
static Log log = LogFactory.getLog(PedidoDAOImpl.class.getName());
	
	private JdbcTemplate jdbcTemplate;
	private int rows;
	private PedidoBean pedidoBean =null;
	
	//TODO - JDBCTEMPLATE SETER
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    
    
	@Override
	public List<PedidoBean> selPedidos() throws Exception {

		if (log.isDebugEnabled()) log.debug("Inicio - PedidoDAOImpl.selPedidos");
		
		List<PedidoBean> lstPedidos= null;
		String SQL ="EXEC dbo.SELPEDIDO;";
		
		try {		
			lstPedidos = jdbcTemplate.query(SQL, new ResultSetExtractor<List<PedidoBean>>(){
	            @Override
	            public List<PedidoBean> extractData(ResultSet rs) throws SQLException, DataAccessException
	            {
	            	List<PedidoBean> list = new ArrayList<PedidoBean>();
	            	while (rs.next())
	                {
	                	PedidoBean c = new PedidoBean();
	                	c.setPed_codigo(rs.getString("ped_codigo"));
	                	c.setPed_fecemi(rs.getString("ped_fecemi"));
	                	c.setPed_clicod(rs.getString("ped_clicod"));
	                	c.setPed_estado(rs.getInt("ped_estado"));
	                    list.add(c);
	                }
	                return list;
	            }

	        });	    
		
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - PedidoDAOImpl.selPedidos");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - PedidoDAOImpl.selPedidos");
		}
	
		return lstPedidos;
	
		
	}

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public PedidoBean selPedido(String ped_codigo) throws Exception {

		if (log.isDebugEnabled()) log.debug("Inicio - PedidoDAOImpl.selPedido");
		
		 String SQL = "EXEC dbo.SELPEDIDOXCOD ?;";
		 try {		
				pedidoBean= (PedidoBean)jdbcTemplate.queryForObject(SQL, new Object[] {ped_codigo},new RowMapper(){
		            @Override
		            public PedidoBean mapRow(ResultSet rs,int rowNum) throws SQLException
		            {
		        
		                	PedidoBean c = new PedidoBean();
		                	c.setPed_codigo(rs.getString("ped_codigo"));
		                	c.setPed_fecemi(rs.getString("ped_fecemi"));
		                	c.setPed_clicod(rs.getString("ped_clicod"));
		                	c.setPed_estado(rs.getInt("ped_estado"));
		             
		                return c;
		            }

		        });	    
			
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - PedidoDAOImpl.selPedido");
				log.error(ex, ex);
				throw ex;
			}finally{
				if (log.isDebugEnabled()) log.debug("Final - PedidoDAOImpl.selPedido");
			}
		 	
		 	if(log.isDebugEnabled()) log.debug("Final - PedidoDAOImpl.selPedido");
			
		 	return pedidoBean;
		
	}

	@Override
	public boolean insPedido(PedidoBean newPedido) throws Exception {
	
if (log.isDebugEnabled()) log.debug("Inicio - PedidoDAOImpl.insPedido");
		
		String SQL = "EXEC INSPEDIDO ?,?";
		try {
			rows = jdbcTemplate.update(SQL, new Object[] {
				newPedido.getPed_clicod(),
				newPedido.getPed_estado(),
				});	
				
			
		}catch(Exception ex) {
			
			if (log.isDebugEnabled()) log.debug("Error - PedidoDAOImpl.insPedido");
			log.error(ex, ex);
			throw ex;
		}finally {
			
			if (log.isDebugEnabled()) log.debug("Final - PedidoDAOImpl.insPedido");
		}
		
		
		return rows==1;
		
		
	}

	@Override
	public boolean updPedido(PedidoBean updPedido) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - PedidoDAOImpl.updPedido");
		String SQL = "exec UPDPEDIDO ?,?,?,?;";
		try {
			rows = jdbcTemplate.update(SQL, new Object[] {
					updPedido.getPed_fecemi(),
					updPedido.getPed_clicod(),
					updPedido.getPed_estado(),
					updPedido.getPed_codigo()});	
				
			
		}catch(Exception ex) {
			
			if (log.isDebugEnabled()) log.debug("Error - PedidoDAOImpl.updPedido");
			log.error(ex, ex);
			throw ex;
		}finally {
			if (log.isDebugEnabled()) log.debug("Final - PedidoDAOImpl.updPedido");
		}
		return rows==1 ;
		
		
	}

	@Override
	public boolean delPedido(String ped_codigo) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - PedidoDAOImpl.delPedido");
		String SQL = "EXECUTE DELETEPEDIDODETALLE ? ";
		int rows;
		try {
			rows = jdbcTemplate.update(SQL, new Object[] { ped_codigo });
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - PedidoDAOImpl.delPedido");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - PedidoDAOImpl.delPedido");
		}	     
		
		return rows==1;
		
	}

}
