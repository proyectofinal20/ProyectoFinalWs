package idat.edu.pe.proyecto.dao;
import java.util.List;
import idat.edu.pe.proyecto.bean.DistribuidorBean;


public interface DistribuidorDAO {

	public List<DistribuidorBean> selDistribuidors() throws Exception;
	public DistribuidorBean selDistribuidor(String dist_codigo) throws Exception;
	public DistribuidorBean logDistribuidor(String dist_usuario,String dist_password) throws Exception;
	public boolean insDistribuidor(DistribuidorBean newDistribuidor) throws Exception;
	public boolean updDistribuidor(DistribuidorBean updDistribuidor) throws Exception;
	public boolean delDistribuidor(String dist_codigo) throws Exception;
	
	
}
