package idat.edu.pe.proyecto.dao;

import java.util.List;

import idat.edu.pe.proyecto.bean.RutaBean;

public interface RutaDAO {
	public List<RutaBean> selRutas() throws Exception;
}
