package idat.edu.pe.proyecto.dao;

import java.util.List;

import idat.edu.pe.proyecto.bean.ArticuloBean;

public interface ArticuloDAO {

	public List<ArticuloBean> selArticulos() throws Exception;
	public ArticuloBean selArticulo(String art_codigo) throws Exception;
	public boolean insArticulo(ArticuloBean newArticulo) throws Exception;
	public boolean updArticulo(ArticuloBean updArticulo) throws Exception;
	public boolean delArticulo(String art_codigo) throws Exception;
	
}
