package idat.edu.pe.proyecto.dao;

import java.util.List;

import idat.edu.pe.proyecto.bean.PedidoBean;

public interface PedidoDAO {

	public List<PedidoBean> selPedidos() throws Exception;
	public PedidoBean selPedido(String ped_codigo) throws Exception;
	public boolean insPedido(PedidoBean newPedido) throws Exception;
	public boolean updPedido(PedidoBean updPedido) throws Exception;
	public boolean delPedido(String ped_codigo) throws Exception;
	
	
}
