package idat.edu.pe.proyecto.dao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import idat.edu.pe.proyecto.bean.ClienteBean;

public class ClienteDAOImpl implements ClienteDAO{

static Log log = LogFactory.getLog(ClienteDAOImpl.class.getName());
	
	private JdbcTemplate jdbcTemplate;
	private int rows;
	private ClienteBean clienteBean =null;
	
	
	
	
	//TODO - JDBCTEMPLATE SETER
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
	

	@Override
	public List<ClienteBean> selClientes() throws Exception {
		

		if (log.isDebugEnabled()) log.debug("Inicio - ClienteDAOImpl.selClientes");
		
		List<ClienteBean> lstClientes= null;
		String SQL ="SELECT * FROM CLIENTE";

		try {		
			lstClientes = jdbcTemplate.query(SQL, new ResultSetExtractor<List<ClienteBean>>(){
	            @Override
	            public List<ClienteBean> extractData(ResultSet rs) throws SQLException, DataAccessException
	            {
	            	List<ClienteBean> list = new ArrayList<ClienteBean>();
	            	while (rs.next())
	                {
	                	ClienteBean c = new ClienteBean();
	                	c.setCli_codigo(rs.getString("cli_codigo"));
	                	c.setCli_apellido(rs.getString("cli_apellido"));
	                	c.setCli_nombre(rs.getString("cli_nombre"));
	                	c.setCli_movil1(rs.getString("cli_movil1"));
	                	c.setCli_movil2(rs.getString("cli_movil2"));
	                	c.setCli_email(rs.getString("cli_email"));
	                	c.setCli_direc(rs.getString("cli_direc"));
	                	c.setCli_ubigeo(rs.getString("cli_ubigeo"));
	                	c.setCli_imagen(rs.getString("cli_imagen"));
	                	c.setCli_latitud(rs.getString("cli_latitud"));
	                	c.setCli_longitud(rs.getString("cli_longitud"));
	                	c.setCli_estado(rs.getInt("cli_estado"));
	                    list.add(c);
	                }
	                return list;
	            }

	        });	    
		
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - ClienteDAOImpl.selClientes");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.selClientes");
		}
	
		return lstClientes;
			
		
		
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ClienteBean selCliente(String cli_codigo) throws Exception {
		
		if (log.isDebugEnabled()) log.debug("Inicio - ClienteDAOImpl.selCliente");
		
		 String SQL = "SELECT * FROM CLIENTE WHERE cli_codigo = ? ";
		 try {		
				clienteBean= (ClienteBean)jdbcTemplate.queryForObject(SQL, new Object[] {cli_codigo},new RowMapper(){
		            @Override
		            public ClienteBean mapRow(ResultSet rs,int rowNum) throws SQLException
		            {
		        
		                	ClienteBean c = new ClienteBean();
		                	c.setCli_codigo(rs.getString("cli_codigo"));
		                	c.setCli_apellido(rs.getString("cli_apellido"));
		                	c.setCli_nombre(rs.getString("cli_nombre"));
		                	c.setCli_movil1(rs.getString("cli_movil1"));
		                	c.setCli_movil2(rs.getString("cli_movil2"));
		                	c.setCli_email(rs.getString("cli_email"));
		                	c.setCli_direc(rs.getString("cli_direc"));
		                	c.setCli_ubigeo(rs.getString("cli_ubigeo"));
		                	c.setCli_imagen(rs.getString("cli_imagen"));
		                	c.setCli_latitud(rs.getString("cli_latitud"));
		                	c.setCli_longitud(rs.getString("cli_longitud"));
		                	c.setCli_estado(rs.getInt("cli_estado"));
		             
		                return c;
		            }

		        });	    
			
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - ClienteDAOImpl.selCliente");
				log.error(ex, ex);
				throw ex;
			}finally{
				if (log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.selCliente");
			}
		 	
		 	if(log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.selCliente");
			
		 	return clienteBean;
		
		
		
	}

	@Override
	public boolean insCliente(ClienteBean newCliente) throws Exception {
if (log.isDebugEnabled()) log.debug("Inicio - ClienteDAOImpl.insCliente");
		
		String SQL = "INSERT INTO CLIENTE VALUES ((select 'C' + right('000' + ltrim(right(isnull(max(cli_codigo),'C0000'),4) +1),4) from CLIENTE),?,?,?,?,?,?,?,?,?,?,?);";
		try {
			rows = jdbcTemplate.update(SQL, new Object[] {
				newCliente.getCli_apellido(),
				newCliente.getCli_nombre(),
				newCliente.getCli_movil1(),
				newCliente.getCli_movil2(),
				newCliente.getCli_email(),
				newCliente.getCli_direc(),
				newCliente.getCli_ubigeo(),
				newCliente.getCli_imagen(),
				newCliente.getCli_latitud(),
				newCliente.getCli_longitud(),
				newCliente.getCli_estado()});	
				
			
		}catch(Exception ex) {
			
			if (log.isDebugEnabled()) log.debug("Error - ClienteDAOImpl.insCliente");
			log.error(ex, ex);
			throw ex;
		}finally {
			
			if (log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.insCliente");
		}
		
		
		return rows==1;
		
		
	}

	@Override
	public boolean updCliente(ClienteBean updCliente) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - ClienteDAOImpl.updCliente");
		String SQL = "update CLIENTE set cli_apellido=?,cli_nombre=?,cli_movil1=?,cli_movil2=?,cli_email=?,cli_direc=?,cli_ubigeo=?,cli_imagen=?,cli_latitud=?,cli_longitud=?,cli_estado=? where cli_codigo=?;";
		try {
			rows = jdbcTemplate.update(SQL, new Object[] {
					updCliente.getCli_apellido(),
					updCliente.getCli_nombre(),
					updCliente.getCli_movil1(),
					updCliente.getCli_movil2(),
					updCliente.getCli_email(),
					updCliente.getCli_direc(),
					updCliente.getCli_ubigeo(),
					updCliente.getCli_imagen(),
					updCliente.getCli_latitud(),
					updCliente.getCli_longitud(),
					updCliente.getCli_estado(),
					updCliente.getCli_codigo()});	
				
			
		}catch(Exception ex) {
			
			if (log.isDebugEnabled()) log.debug("Error - ClienteDAOImpl.updCliente");
			log.error(ex, ex);
			throw ex;
		}finally {
			if (log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.updCliente");
		}
		return rows==1 ;
		
		
		
		
	}

	@Override
	public boolean delCliente(String cli_codigo) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - ClienteDAOImpl.delCliente");
		String SQL = "DELETE FROM CLIENTE WHERE cli_codigo=? ";
		int rows;
		try {
			rows = jdbcTemplate.update(SQL, new Object[] { cli_codigo });
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - ClienteDAOImpl.delCliente");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.delCliente");
		}	     
		
		return rows==1;
		
		
		
	}

}
