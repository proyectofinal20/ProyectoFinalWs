package idat.edu.pe.proyecto.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import idat.edu.pe.proyecto.bean.ArticuloBean;


public class ArticuloDAOImpl implements ArticuloDAO {
	
static Log log = LogFactory.getLog(ArticuloDAOImpl.class.getName());
	
	private JdbcTemplate jdbcTemplate;
	private int rows;
	private ArticuloBean articuloBean =null;
	

	//TODO - JDBCTEMPLATE SETER
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
	

	@Override
	public List<ArticuloBean> selArticulos() throws Exception {
		
	if (log.isDebugEnabled()) log.debug("Inicio - ArticuloDAOImpl.selArticulos");
		
		List<ArticuloBean> lstArticulos= null;
		String SQL ="SELECT * FROM ARTICULO";

		try {		
			lstArticulos = jdbcTemplate.query(SQL, new ResultSetExtractor<List<ArticuloBean>>(){
	            @Override
	            public List<ArticuloBean> extractData(ResultSet rs) throws SQLException, DataAccessException
	            {
	            	List<ArticuloBean> list = new ArrayList<ArticuloBean>();
	            	while (rs.next())
	                {
	                	ArticuloBean c = new ArticuloBean();
	                	c.setArt_codigo(rs.getString("art_codigo"));
	                	c.setArt_descripcion(rs.getString("art_descripcion"));
	                	c.setArt_precunidad(rs.getDouble("arti_precunidad"));
	                	c.setArt_estado(rs.getInt("art_estado"));
	  
	                    list.add(c);
	                }
	                return list;
	            }

	        });	    
		
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - ArticuloDAOImpl.selArticulos");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - ArticuloDAOImpl.selArticulos");
		}
	
		return lstArticulos;
			
		
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public ArticuloBean selArticulo(String art_codigo) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - ArticuloDAOImpl.selArticulo");
		
		 String SQL = "SELECT * FROM ARTICULO WHERE art_codigo = ? ";
		 try {		
				articuloBean= (ArticuloBean)jdbcTemplate.queryForObject(SQL, new Object[] {art_codigo},new RowMapper(){
		            @Override
		            public ArticuloBean mapRow(ResultSet rs,int rowNum) throws SQLException
		            {
		        
		                	ArticuloBean c = new ArticuloBean();
		                	c.setArt_codigo(rs.getString("art_codigo"));
		                	c.setArt_descripcion(rs.getString("art_descripcion"));
		                	c.setArt_precunidad(rs.getDouble("arti_precunidad"));
		                	c.setArt_estado(rs.getInt("art_estado"));
		             
		                return c;
		            }

		        });	    
			
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - ArticuloDAOImpl.selArticulo");
				log.error(ex, ex);
				throw ex;
			}finally{
				if (log.isDebugEnabled()) log.debug("Final - ArticuloDAOImpl.selArticulo");
			}
		 	
		 	if(log.isDebugEnabled()) log.debug("Final - ArticuloDAOImpl.selArticulo");
			
		 	return articuloBean;
		
		
	}

	@Override
	public boolean insArticulo(ArticuloBean newArticulo) throws Exception {
if (log.isDebugEnabled()) log.debug("Inicio - ArticuloDAOImpl.insArticulo");
		
		String SQL = "INSERT INTO ARTICULO VALUES ((select 'A' + right('000' + ltrim(right(isnull(max(art_codigo),'A0000'),4) +1),4) from ARTICULO),?,?,?);";
		try {
			rows = jdbcTemplate.update(SQL, new Object[] {
				newArticulo.getArt_descripcion(),
				newArticulo.getArt_precunidad(),
				newArticulo.getArt_estado()});	
				
			
		}catch(Exception ex) {
			
			if (log.isDebugEnabled()) log.debug("Error - ClienteDAOImpl.insCliente");
			log.error(ex, ex);
			throw ex;
		}finally {
			
			if (log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.insCliente");
		}
		
		
		return rows==1;
		
	}

	@Override
	public boolean updArticulo(ArticuloBean updArticulo) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - ClienteDAOImpl.updCliente");
		String SQL = "update ARTICULO set art_descripcion=?,arti_precunidad=?,art_estado=? where art_codigo=?;";
		try {
			rows = jdbcTemplate.update(SQL, new Object[] {
					updArticulo.getArt_codigo(),
					updArticulo.getArt_descripcion(),
					updArticulo.getArt_precunidad(),
					updArticulo.getArt_estado()});	
				
			
		}catch(Exception ex) {
			
			if (log.isDebugEnabled()) log.debug("Error - ClienteDAOImpl.updCliente");
			log.error(ex, ex);
			throw ex;
		}finally {
			if (log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.updCliente");
		}
		return rows==1 ;
	}

	@Override
	public boolean delArticulo(String art_codigo) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - ClienteDAOImpl.delCliente");
		String SQL = "DELETE FROM ARTICULO WHERE art_codigo=? ";
		int rows;
		try {
			rows = jdbcTemplate.update(SQL, new Object[] { art_codigo });
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - ClienteDAOImpl.delCliente");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - ClienteDAOImpl.delCliente");
		}	     
		
		return rows==1;
		
		
	}

}
