package idat.edu.pe.proyecto.dao;

import java.util.List;

import idat.edu.pe.proyecto.bean.PedidoDetalleBean;

public interface PedidoDetalleDAO {
	public List<PedidoDetalleBean> selPedidosDetalle() throws Exception;
	public List<PedidoDetalleBean> selPedidosDetallexCod(String codped) throws Exception;
	public boolean insPedidoDetalle(PedidoDetalleBean newPedido) throws Exception;
}
