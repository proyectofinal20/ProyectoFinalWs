package idat.edu.pe.proyecto.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import idat.edu.pe.proyecto.bean.RepartoBean;


public class RepartoDAOImpl implements RepartoDAO{

static Log log = LogFactory.getLog(RepartoDAOImpl.class.getName());
	
	private JdbcTemplate jdbcTemplate;
	private int rows;
	private RepartoBean repartoBean =null;
	
	//TODO - JDBCTEMPLATE SETER
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
	
	
	
	@Override
	public List<RepartoBean> selRepartos() throws Exception {
	if (log.isDebugEnabled()) log.debug("Inicio - RepartoDAOImpl.selRepartos");
		
		List<RepartoBean> lstRepartos= null;
		String SQL ="EXECUTE SELREPARTODETALLE;";
		
		try {		
			lstRepartos = jdbcTemplate.query(SQL, new ResultSetExtractor<List<RepartoBean>>(){
	            @Override
	            public List<RepartoBean> extractData(ResultSet rs) throws SQLException, DataAccessException
	            {
	            	List<RepartoBean> list = new ArrayList<RepartoBean>();
	            	while (rs.next())
	                {
	                	RepartoBean c = new RepartoBean();
	                	c.setRep_codigo(rs.getString("rep_codigo"));
	                	c.setRep_fecemi(rs.getString("rep_fecemi"));
	                	c.setRep_observ(rs.getString("rep_observ"));
	                	c.setDist_codigo(rs.getString("dist_codigo"));
	                	c.setRep_estado(rs.getInt("rep_estado"));
	                	c.setRepd_repcod(rs.getString("repd_repcod"));
	                	c.setRepd_pedcod(rs.getString("repd_pedcod"));
	                	c.setRepd_estado(rs.getInt("repd_estado"));
	                	
	                    list.add(c);
	                }
	                return list;
	            }

	        });	    
		
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - RepartoDAOImpl.selRepartos");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - RepartoDAOImpl.selRepartos");
		}
	
		return lstRepartos;
	
	}

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public RepartoBean selReparto(String rep_codigo) throws Exception {

		if (log.isDebugEnabled()) log.debug("Inicio - RepartoDAOImpl.selReparto");
		
		 String SQL = "EXECUTE SELREPARTODETALLEXCOD ?;";
		 try {		
				repartoBean= (RepartoBean)jdbcTemplate.queryForObject(SQL, new Object[] {rep_codigo},new RowMapper(){
		            @Override
		            public RepartoBean mapRow(ResultSet rs,int rowNum) throws SQLException
		            {
		        
		                	RepartoBean c = new RepartoBean();
		                	c.setRep_codigo(rs.getString("rep_codigo"));
		                	c.setRep_fecemi(rs.getString("rep_fecemi"));
		                	c.setRep_observ(rs.getString("rep_observ"));
		                	c.setDist_codigo(rs.getString("dist_codigo"));
		                	c.setRep_estado(rs.getInt("rep_estado"));
		                	c.setRepd_repcod(rs.getString("repd_repcod"));
		                	c.setRepd_pedcod(rs.getString("repd_pedcod"));
		                	c.setRepd_estado(rs.getInt("repd_estado"));
		             
		                return c;
		            }

		        });	    
			
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - RepartoDAOImpl.selReparto");
				log.error(ex, ex);
				throw ex;
			}finally{
				if (log.isDebugEnabled()) log.debug("Final - RepartoDAOImpl.selReparto");
			}
		 	
		 	if(log.isDebugEnabled()) log.debug("Final - RepartoDAOImpl.selReparto");
			
		 	return repartoBean;
		
		
	}

	@Override
	public boolean insReparto(RepartoBean newReparto) throws Exception {
if (log.isDebugEnabled()) log.debug("Inicio - RepartoDAOImpl.insReparto");
		
		String SQL = "exec INSREPARTODETALLE ?,?,?,?,?,?;";
		try {
			rows = jdbcTemplate.update(SQL, new Object[] {
				newReparto.getRep_fecemi(),
				newReparto.getRep_observ(),
				newReparto.getDist_codigo(),
				newReparto.getRep_estado(),
				newReparto.getRepd_pedcod(),
				newReparto.getRepd_estado()});	
				
			
		}catch(Exception ex) {
			
			if (log.isDebugEnabled()) log.debug("Error - RepartoDAOImpl.insReparto");
			log.error(ex, ex);
			throw ex;
		}finally {
			
			if (log.isDebugEnabled()) log.debug("Final - RepartoDAOImpl.insReparto");
		}
		
		
		return rows==1;
		
	}

	@Override
	public boolean updReparto(RepartoBean updReparto) throws Exception {
		
		if (log.isDebugEnabled()) log.debug("Inicio - RepartoDAOImpl.updReparto");
		String SQL = "exec UPDREPARTODETALLE ?,?,?,?,?,?,?;";
		try {
			rows = jdbcTemplate.update(SQL, new Object[] {
					updReparto.getRep_fecemi(),
					updReparto.getRep_observ(),
					updReparto.getDist_codigo(),
					updReparto.getRep_estado(),
					updReparto.getRepd_pedcod(),
					updReparto.getRepd_estado(),
					updReparto.getRepd_repcod()});	
				
			
		}catch(Exception ex) {
			
			if (log.isDebugEnabled()) log.debug("Error - RepartoDAOImpl.updReparto");
			log.error(ex, ex);
			throw ex;
		}finally {
			if (log.isDebugEnabled()) log.debug("Final - RepartoDAOImpl.updReparto");
		}
		return rows==1 ;
		
		
		
		
	}

	@Override
	public boolean delReparto(String rep_codigo) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - RepartoDAOImpl.delReparto");
		String SQL = "EXECUTE DELETEREPARTODETALLE ?";
		int rows;
		try {
			rows = jdbcTemplate.update(SQL, new Object[] { rep_codigo });
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - RepartoDAOImpl.delReparto");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - RepartoDAOImpl.delReparto");
		}	     
		
		return rows==1;
		
	}
	
	@Override
	public List<RepartoBean> selRutas() throws Exception {
	if (log.isDebugEnabled()) log.debug("Inicio - RepartoDAOImpl.selRepartos");
		
		List<RepartoBean> lstRepartos= null;
		String SQL ="EXEC SELDATECOUNTREPARTO;";
		
		try {		
			lstRepartos = jdbcTemplate.query(SQL, new ResultSetExtractor<List<RepartoBean>>(){
	            @Override
	            public List<RepartoBean> extractData(ResultSet rs) throws SQLException, DataAccessException
	            {
	            	List<RepartoBean> list = new ArrayList<RepartoBean>();
	            	while (rs.next())
	                {
	                	RepartoBean c = new RepartoBean();
	                	c.setRuta_fecha(rs.getString("RUTA_FECHA"));
	                	c.setRuta_nrocliente(rs.getString("RUTA_NROCLIENTES"));
	                    list.add(c);
	                }
	                return list;
	            }

	        });	    
		
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - RepartoDAOImpl.selRepartos");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - RepartoDAOImpl.selRepartos");
		}
	
		return lstRepartos;
	
	}

}
