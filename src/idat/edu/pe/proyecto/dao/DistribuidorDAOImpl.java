package idat.edu.pe.proyecto.dao;

import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;


import idat.edu.pe.proyecto.bean.DistribuidorBean;

public class DistribuidorDAOImpl implements DistribuidorDAO{
	
static Log log = LogFactory.getLog(DistribuidorDAOImpl.class.getName());
	
	private JdbcTemplate jdbcTemplate;
	private int rows;
	private DistribuidorBean distribuidorBean =null;
	

	
	//TODO - JDBCTEMPLATE SETER
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
	
	
	
	@Override
	public List<DistribuidorBean> selDistribuidors() throws Exception {
if (log.isDebugEnabled()) log.debug("Inicio - DistribuidorDAOImpl.selDistribuidor");
		
		List<DistribuidorBean> lstDistribuidors= null;
		String SQL ="SELECT * FROM DISTRIBUIDOR";

		try {		
			lstDistribuidors = jdbcTemplate.query(SQL, new ResultSetExtractor<List<DistribuidorBean>>(){
	            @Override
	            public List<DistribuidorBean> extractData(ResultSet rs) throws SQLException, DataAccessException
	            {
	            	List<DistribuidorBean> list = new ArrayList<DistribuidorBean>();
	            	while (rs.next())
	                {
	                	DistribuidorBean c = new DistribuidorBean();
	                	c.setDist_codigo(rs.getString("dist_codigo"));
	                	c.setDist_usuario(rs.getString("dist_usuario").trim());
	                	c.setDist_password(rs.getString("dist_password"));
	                	c.setDist_estado(rs.getInt("dist_estado"));
	                    list.add(c);
	                }
	                return list;
	            }

	        });	    
		
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - DistribuidorDAOImpl.selDistribuidors");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - DistribuidorDAOImpl.selDistribuidors");
		}
	
		return lstDistribuidors;
			
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public DistribuidorBean selDistribuidor(String dist_codigo) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - DistribuidorDAOImpl.selDistribuidor");
		
		 String SQL = "SELECT * FROM DISTRIBUIDOR WHERE dist_codigo = ? ";
		 try {		
				distribuidorBean= (DistribuidorBean)jdbcTemplate.queryForObject(SQL, new Object[] {dist_codigo},new RowMapper(){
		            @Override
		            public DistribuidorBean mapRow(ResultSet rs,int rowNum) throws SQLException
		            {
		        
		                	DistribuidorBean c = new DistribuidorBean();
		                	c.setDist_codigo(rs.getString("dist_codigo"));
		                	c.setDist_usuario(rs.getString("dist_usuario"));
		                	c.setDist_password(rs.getString("dist_password"));
		                	c.setDist_estado(rs.getInt("dist_estado"));
		                    
		             
		                return c;
		            }

		        });	    
			
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - DistribuidorDAOImpl.selDistribuidor");
				log.error(ex, ex);
				throw ex;
			}finally{
				if (log.isDebugEnabled()) log.debug("Final - DistribuidorDAOImpl.selDistribuidor");
			}
		 	
		 	if(log.isDebugEnabled()) log.debug("Final - DistribuidorDAOImpl.selDistribuidor");
			
		 	return distribuidorBean;
	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public DistribuidorBean logDistribuidor(String dist_usuario, String dist_password) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - DistribuidorDAOImpl.selDistribuidor");
		
		 String SQL = "\r\n" + 
		 		"SELECT * FROM DISTRIBUIDOR where dist_usuario=? COLLATE SQL_Latin1_General_Cp1_CS_AS and CONVERT(VARCHAR(MAX), DECRYPTBYPASSPHRASE('passdistribuidor', dist_password)) LIKE ? COLLATE SQL_Latin1_General_Cp1_CS_AS";
		 try {		
				distribuidorBean= (DistribuidorBean)jdbcTemplate.queryForObject(SQL, new Object[] {dist_usuario,dist_password},new RowMapper(){
		            @Override
		            public DistribuidorBean mapRow(ResultSet rs,int rowNum) throws SQLException
		            {
		        
		                	DistribuidorBean c = new DistribuidorBean();
		                	c.setDist_codigo(rs.getString("dist_codigo"));
		                	c.setDist_usuario(rs.getString("dist_usuario"));
		                	c.setDist_password(rs.getString("dist_password"));
		                	c.setDist_estado(rs.getInt("dist_estado"));
		                    
		                    
		             
		                return c;
		            }

		        });	    
			
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - DistribuidorDAOImpl.LoginDistribuidor");
				log.error(ex, ex);
				throw ex;
			}finally{
				if (log.isDebugEnabled()) log.debug("Final - DistribuidorDAOImpl.LoginDistribuidor");
			}
		 	
		 	if(log.isDebugEnabled()) log.debug("Final - DistribuidorDAOImpl.LoginDistribuidor");
			
		 	return distribuidorBean;
	
	}

	@Override
	public boolean insDistribuidor(DistribuidorBean newDistribuidor) throws Exception {
if (log.isDebugEnabled()) log.debug("Inicio - DistribuidorDAOImpl.insDistribuidor");
		
		String SQL = "INSERT INTO DISTRIBUIDOR VALUES ((select 'D' + right('000' + ltrim(right(isnull(max(dist_codigo),'D0000'),4) +1),4) from DISTRIBUIDOR),?,ENCRYPTBYPASSPHRASE('passdistribuidor',?),?);";
		try {
			rows = jdbcTemplate.update(SQL, new Object[] {
				newDistribuidor.getDist_usuario(),
				newDistribuidor.getDist_password(),
				newDistribuidor.getDist_estado()});	
				
			
		}catch(Exception ex) {
			
			if (log.isDebugEnabled()) log.debug("Error - DistribuidorDAOImpl.insDistribuidor");
			log.error(ex, ex);
			throw ex;
		}finally {
			
			if (log.isDebugEnabled()) log.debug("Final - DistribuidorDAOImpl.insDistribuidor");
		}
		
		
		return rows==1;
	}

	@Override
	public boolean updDistribuidor(DistribuidorBean updDistribuidor) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - DistribuidorDAOImpl.updDistribuidor");
		String SQL = "update DISTRIBUIDOR set dist_usuario=?,dist_password=ENCRYPTBYPASSPHRASE('passdistribuidor',?),dist_estado=?  where dist_codigo=?;";
		try {
			rows = jdbcTemplate.update(SQL, new Object[] {
					updDistribuidor.getDist_usuario(),
					updDistribuidor.getDist_password(),
					updDistribuidor.getDist_estado(),	
					updDistribuidor.getDist_codigo()});	
			
		}catch(Exception ex) {
			
			if (log.isDebugEnabled()) log.debug("Error - DistribuidorDAOImpl.updDistribuidor");
			log.error(ex, ex);
			throw ex;
		}finally {
			if (log.isDebugEnabled()) log.debug("Final - DistribuidorDAOImpl.updDistribuidor");
		}
		return rows==1 ;
	}

	@Override
	public boolean delDistribuidor(String dist_codigo) throws Exception {
		if (log.isDebugEnabled()) log.debug("Inicio - DistribuidorDAOImpl.delDistribuidor");
		String SQL = "DELETE FROM DISTRIBUIDOR WHERE dist_codigo=? ";
		int rows;
		try {
			rows = jdbcTemplate.update(SQL, new Object[] { dist_codigo });
		} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - DistribuidorDAOImpl.delDistribuidor");
				log.error(ex, ex);
				throw ex;
		} finally {
			if (log.isDebugEnabled()) log.debug("Final - DistribuidorDAOImpl.delDistribuidor");
		}	     
		
		return rows==1;
	}

}
