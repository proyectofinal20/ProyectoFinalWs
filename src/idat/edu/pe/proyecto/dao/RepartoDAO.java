package idat.edu.pe.proyecto.dao;

import java.util.List;

import idat.edu.pe.proyecto.bean.RepartoBean;

public interface RepartoDAO {
	
	public List<RepartoBean> selRepartos() throws Exception;
	public List<RepartoBean> selRutas() throws Exception;
	public RepartoBean selReparto(String rep_codigo) throws Exception;
	public boolean insReparto(RepartoBean newReparto) throws Exception;
	public boolean updReparto(RepartoBean updReparto) throws Exception;
	public boolean delReparto(String rep_codigo) throws Exception;

}
