package idat.edu.pe.proyecto.dao;
import java.util.List;

import idat.edu.pe.proyecto.bean.ClienteBean;

public interface ClienteDAO {

	public List<ClienteBean> selClientes() throws Exception;
	public ClienteBean selCliente(String cli_codigo) throws Exception;
	public boolean insCliente(ClienteBean newCliente) throws Exception;
	public boolean updCliente(ClienteBean updCliente) throws Exception;
	public boolean delCliente(String cli_codigo) throws Exception;
		
}
