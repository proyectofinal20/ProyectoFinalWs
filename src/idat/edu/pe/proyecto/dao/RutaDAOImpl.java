package idat.edu.pe.proyecto.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import idat.edu.pe.proyecto.bean.RutaBean;

public class RutaDAOImpl implements RutaDAO {

	static Log log = LogFactory.getLog(RutaDAOImpl.class.getName());
	private JdbcTemplate jdbcTemplate;
	
	//TODO - JDBCTEMPLATE SETER
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
	

	@Override
	public List<RutaBean> selRutas() throws Exception {
if (log.isDebugEnabled()) log.debug("Inicio - PedidoDetalleDAOImpl.selRutasDetalle");
		
		List<RutaBean> lstPedidos= null;
		String SQL ="EXEC dbo.SELRUTADETALLE;";
		
		try {		
			lstPedidos = jdbcTemplate.query(SQL,new ResultSetExtractor<List<RutaBean>>(){
	            @Override
	            public List<RutaBean> extractData(ResultSet rs) throws SQLException, DataAccessException
	            {
	            	List<RutaBean> list = new ArrayList<RutaBean>();
	            	while (rs.next())
	                {
	            		RutaBean c = new RutaBean();
	                	c.setRt_codigo(rs.getString("rt_codigo"));
	                	c.setRt_clicod(rs.getString("rt_clicod"));
	                	c.setRt_latitud(rs.getDouble("rt_latitud"));
	                	c.setRt_longitud(rs.getDouble("rt_longitud"));
	                	c.setRt_direc(rs.getString("rt_direc"));
	                	c.setRt_movil1(rs.getString("rt_movil1"));
	                	c.setRt_movil2(rs.getString("rt_movil2"));
	                	
	                    list.add(c);
	                }
	                return list;
	            }

	        });	    
		
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - RutaDAOImpl.selRutas");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - RutaDAOImpl.selRutas");
		}
	
		return lstPedidos;
	}

}
