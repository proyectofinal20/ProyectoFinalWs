package idat.edu.pe.proyecto.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import idat.edu.pe.proyecto.bean.PedidoDetalleBean;

public class PedidoDetalleDAOImpl implements PedidoDetalleDAO {

	static Log log = LogFactory.getLog(PedidoDetalleDAOImpl.class.getName());
	private JdbcTemplate jdbcTemplate;
	private int rows;
	
	//TODO - JDBCTEMPLATE SETER
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
	
	
	@Override
	public List<PedidoDetalleBean> selPedidosDetalle() throws Exception {
if (log.isDebugEnabled()) log.debug("Inicio - PedidoDetalleDAOImpl.selPedidosDetalle");
		
		List<PedidoDetalleBean> lstPedidos= null;
		String SQL ="EXECUTE SELPEDIDODETALLE;";
		
		try {		
			lstPedidos = jdbcTemplate.query(SQL, new ResultSetExtractor<List<PedidoDetalleBean>>(){
	            @Override
	            public List<PedidoDetalleBean> extractData(ResultSet rs) throws SQLException, DataAccessException
	            {
	            	List<PedidoDetalleBean> list = new ArrayList<PedidoDetalleBean>();
	            	while (rs.next())
	                {
	            		PedidoDetalleBean c = new PedidoDetalleBean();
	                	c.setPedd_codigo(rs.getString("pedd_codigo"));
	                	c.setPedd_artcod(rs.getString("pedd_artcod"));
	                	c.setPedd_cantidad(rs.getInt("pedd_cantidad"));
	                	c.setPedd_precio(rs.getDouble("pedd_precio"));
	                	c.setPedd_estado(rs.getInt("pedd_estado"));
	                	
	                    list.add(c);
	                }
	                return list;
	            }

	        });	    
		
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - PedidoDAOImpl.selPedidos");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - PedidoDAOImpl.selPedidos");
		}
	
		return lstPedidos;
	}
	
	@Override
	public List<PedidoDetalleBean> selPedidosDetallexCod(String codped) throws Exception {
if (log.isDebugEnabled()) log.debug("Inicio - PedidoDetalleDAOImpl.selPedidosDetalle");
		
		List<PedidoDetalleBean> lstPedidos= null;
		String SQL ="EXECUTE SELPEDIDODETALLEXCODPEDIDO ?;";
		
		try {		
			lstPedidos = jdbcTemplate.query(SQL, new Object[] {codped}, new ResultSetExtractor<List<PedidoDetalleBean>>(){
	            @Override
	            public List<PedidoDetalleBean> extractData(ResultSet rs) throws SQLException, DataAccessException
	            {
	            	List<PedidoDetalleBean> list = new ArrayList<PedidoDetalleBean>();
	            	while (rs.next())
	                {
	            		PedidoDetalleBean c = new PedidoDetalleBean();
	                	c.setPedd_codigo(rs.getString("pedd_codigo"));
	                	c.setPedd_artcod(rs.getString("pedd_artcod"));
	                	c.setPedd_cantidad(rs.getInt("pedd_cantidad"));
	                	c.setPedd_precio(rs.getDouble("pedd_precio"));
	                	c.setPedd_estado(rs.getInt("pedd_estado"));
	                	
	                    list.add(c);
	                }
	                return list;
	            }

	        });	    
		
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - PedidoDAOImpl.selPedidos");
			log.error(ex, ex);
			throw ex;
		}finally{
			if (log.isDebugEnabled()) log.debug("Final - PedidoDAOImpl.selPedidos");
		}
	
		return lstPedidos;
	}

	@Override
	public boolean insPedidoDetalle(PedidoDetalleBean newPedido) throws Exception {

if (log.isDebugEnabled()) log.debug("Inicio - PedidoDetalleDAOImpl.insPedidoDetalle");
		
		String SQL = "EXEC INSPEDIDODETALLE ?,?,?,?";
		try {
			rows = jdbcTemplate.update(SQL, new Object[] {
				newPedido.getPedd_artcod(),
				newPedido.getPedd_cantidad(),
				newPedido.getPedd_precio(),
				newPedido.getPedd_estado()
				});	
				
			
		}catch(Exception ex) {
			
			if (log.isDebugEnabled()) log.debug("Error - PedidoDAOImpl.insPedido");
			log.error(ex, ex);
			throw ex;
		}finally {
			
			if (log.isDebugEnabled()) log.debug("Final - PedidoDAOImpl.insPedido");
		}
		
		
		return rows==1;
		
	}


}
