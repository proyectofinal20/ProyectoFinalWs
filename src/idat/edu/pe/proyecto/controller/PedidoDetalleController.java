package idat.edu.pe.proyecto.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import idat.edu.pe.proyecto.bean.PedidoDetalleBean;
import idat.edu.pe.proyecto.service.PedidoDetalleService;
import net.sf.sojo.interchange.json.JsonSerializer;

@Controller
@RequestMapping("PedidoDetalle")
public class PedidoDetalleController {
	
	static Log log = LogFactory.getLog(PedidoDetalleController.class.getName());
	@Autowired		
	PedidoDetalleService pedidodetalleService;
	boolean result;
	
	@RequestMapping(value="/Web", method = RequestMethod.GET)
	public ModelAndView showView(HttpServletRequest request, HttpServletResponse response) {
		log.info("PedidoDetalleController.showView - Iniciando.");
		ModelAndView modelo = null;
		//HttpSession session = request.getSession(true);
		try {
			//TODO ModalAndView
			modelo = new ModelAndView();
			modelo.setViewName("viewPedidoDetalle");
			
			Map<String, Object> titulos = new HashMap<String, Object>();		
			titulos.put("tituloDefecto", "Mantenimiento Pedido");			
			
			modelo.addObject("titulos", new JsonSerializer().serialize(titulos));
			
			//TODO LISTAR CARGOS 		
			List<PedidoDetalleBean> lstPedidos = pedidodetalleService.selPedidosDetalle();		
						
			modelo.addObject("listadoPedidos", new JsonSerializer().serialize(lstPedidos));			
					
		} catch (Exception e) {
			log.error("PedidoDetalleController.showView - Error");
			log.error(e.getMessage());
		}
		log.info("PedidoDetalleController.showView - Finalizando");
		return modelo;
	}
	
		@RequestMapping(value="/Rest", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
		public  @ResponseBody Map<String, Object> getPedidoAll(HttpServletRequest request, HttpServletResponse response) throws Exception{
			Map<String, Object> mapaResult = new HashMap<String, Object>();
			try {
				if (log.isDebugEnabled()) log.debug("Inicio - PedidoDetalleController.showView");	
				
				//TODO LISTAR CARGOS 		
				List<PedidoDetalleBean> lstPedidos = pedidodetalleService.selPedidosDetalle();
				
				mapaResult.put("status", true);
				mapaResult.put("message", lstPedidos.size() + " Registros Encontrados");
				mapaResult.put("data", lstPedidos);
				
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - PedidoDetalleController.showView");
				log.error(ex, ex);
				mapaResult.put("status", false);
				mapaResult.put("message", ex.getLocalizedMessage());
			}finally {
				if (log.isDebugEnabled()) log.debug("Fin - PedidoDetalleController.showView");
			}
			
			return mapaResult;
		}
		
		//TODO METHOD GET
		@RequestMapping(value="/Rest/{codped}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
		public @ResponseBody Map<String, Object> getMascota(@PathVariable String codped) {	
			Map<String, Object> mapaResult = new HashMap<String, Object>();
			try {
				if (log.isDebugEnabled()) log.debug("Inicio - PedidoDetalleController.showView");	
				
				//TODO LISTAR CARGOS 		
				List<PedidoDetalleBean> lstPedidodetalle = pedidodetalleService.selPedidosDetallexCod(codped);
				
				mapaResult.put("status", true);
				mapaResult.put("message", lstPedidodetalle.size() + " Registros Encontrados");
				mapaResult.put("data", lstPedidodetalle);
				
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - PedidoDetalleController.showView");
				log.error(ex, ex);
				mapaResult.put("status", false);
				mapaResult.put("message", ex.getLocalizedMessage());
			}finally {
				if (log.isDebugEnabled()) log.debug("Fin - PedidoDetalleController.showView");
			}
			
			return mapaResult;
		}
		
		//TODO METHOD POST
		@RequestMapping(value="/Rest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
		public @ResponseBody Map<String, Object> postPedido(HttpServletRequest request, HttpServletResponse response) throws Exception {
			Map<String, Object> mapaResult = new HashMap<String, Object>();		
			try {
				if (log.isDebugEnabled()) log.debug("Inicio - PedidoDetalleController.postPedido");			
				//LECTURA DEL REQUEST BODY JSON
				BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream(),"utf-8"));
				StringBuilder jsonEnviado = new StringBuilder();
				String line;
		        while ((line = reader.readLine()) != null) {
		        	jsonEnviado.append(line).append('\n');
		        }
		        //----------------------------------------------
		        //TODO SERIALIZAMOS EL JSON -> CLASS 
				PedidoDetalleBean pedidoNuevo = (PedidoDetalleBean) new JsonSerializer().deserialize(jsonEnviado.toString(), PedidoDetalleBean.class);			
				//----------------------------------------------
				//TODO SERVICE INSERTAR USUARIO
				result = pedidodetalleService.insPedidoDetalle(pedidoNuevo);
				//----------------------------------------------
				if(result == true) {
					mapaResult.put("status", true);
					mapaResult.put("message", "1 Registro Guardado");
				}else {
					mapaResult.put("status", false);
					mapaResult.put("message", "0 Registro Guardado");
				}			
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - PedidoDetalleController.postPedido");
				log.error(ex, ex);
				mapaResult.put("status", false);
				mapaResult.put("message", ex.getLocalizedMessage());
			}finally {
				if (log.isDebugEnabled()) log.debug("Fin - PedidoDetalleController.postPedido");			
			}			
			return mapaResult;
		}

}
