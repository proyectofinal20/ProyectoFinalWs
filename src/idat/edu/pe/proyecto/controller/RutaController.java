package idat.edu.pe.proyecto.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import idat.edu.pe.proyecto.bean.RutaBean;
import idat.edu.pe.proyecto.bean.PedidoDetalleBean;
import idat.edu.pe.proyecto.service.RutaService;
import net.sf.sojo.interchange.json.JsonSerializer;

@Controller
@RequestMapping("Ruta")
public class RutaController {
	
	static Log log = LogFactory.getLog(RutaController.class.getName());
	@Autowired		
	RutaService rutaService;
	boolean result;
	
	@RequestMapping(value="/Web", method = RequestMethod.GET)
	public ModelAndView showView(HttpServletRequest request, HttpServletResponse response) {
		log.info("RutaController.showView - Iniciando.");
		ModelAndView modelo = null;
		//HttpSession session = request.getSession(true);
		try {
			//TODO ModalAndView
			modelo = new ModelAndView();
			modelo.setViewName("viewRuta");
			
			Map<String, Object> titulos = new HashMap<String, Object>();		
			titulos.put("tituloDefecto", "Mantenimiento Pedido");			
			
			modelo.addObject("titulos", new JsonSerializer().serialize(titulos));
			
			//TODO LISTAR CARGOS 		
			List<PedidoDetalleBean> lstRutas = null;	
						
			modelo.addObject("listadoRutas", new JsonSerializer().serialize(lstRutas));			
					
		} catch (Exception e) {
			log.error("RutaController.showView - Error");
			log.error(e.getMessage());
		}
		log.info("RutaController.showView - Finalizando");
		return modelo;
	}
	
	
		@RequestMapping(value="/Rest/Listado", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
		public  @ResponseBody Map<String, Object> getRepartoAll(HttpServletRequest request, HttpServletResponse response) throws Exception{
			Map<String, Object> mapaResult = new HashMap<String, Object>();
			try {
				if (log.isDebugEnabled()) log.debug("Inicio - RutaController.showView");	
				
				//TODO LISTAR CARGOS 		
				List<RutaBean> lstRutas = rutaService.selRutas();
				
				mapaResult.put("status", true);
				mapaResult.put("message", lstRutas.size() + " Registros Encontrados");
				mapaResult.put("data", lstRutas);
				
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - RutaController.showView");
				log.error(ex, ex);
				mapaResult.put("status", false);
				mapaResult.put("message", ex.getLocalizedMessage());
			}finally {
				if (log.isDebugEnabled()) log.debug("Fin - RutaController.showView");
			}
			
			return mapaResult;
		}
		

}
