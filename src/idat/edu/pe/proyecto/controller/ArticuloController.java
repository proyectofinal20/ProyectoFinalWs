package idat.edu.pe.proyecto.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import idat.edu.pe.proyecto.bean.ArticuloBean;
import idat.edu.pe.proyecto.service.ArticuloService;
import net.sf.sojo.interchange.json.JsonSerializer;

@Controller
@RequestMapping("Articulo")

public class ArticuloController {
	
	static Log log = LogFactory.getLog(ArticuloController.class.getName());
	@Autowired		
	ArticuloService articuloService;
	boolean result;
	
	@RequestMapping(value="/Web", method = RequestMethod.GET)
	public ModelAndView showView(HttpServletRequest request, HttpServletResponse response) {
		log.info("ArticuloController.showView - Iniciando.");
		ModelAndView modelo = null;
		//HttpSession session = request.getSession(true);
		try {
			//TODO ModalAndView
			modelo = new ModelAndView();
			modelo.setViewName("viewArticulo");
			
			Map<String, Object> titulos = new HashMap<String, Object>();		
			titulos.put("tituloDefecto", "Mantenimiento Articulo");			
			
			modelo.addObject("titulos", new JsonSerializer().serialize(titulos));
			
			//TODO LISTAR CARGOS 		
			List<ArticuloBean> lstArticulos = articuloService.selArticulos();		
						
			modelo.addObject("listadoArticulos", new JsonSerializer().serialize(lstArticulos));			
					
		} catch (Exception e) {
			log.error("ArticuloController.showView - Error");
			log.error(e.getMessage());
		}
		log.info("ArticuloController.showView - Finalizando");
		return modelo;
	}
	
	
	//TODO METHOD GET CARGO ALL	
			@RequestMapping(value="/Rest", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
			public  @ResponseBody Map<String, Object> getArticuloAll(HttpServletRequest request, HttpServletResponse response) throws Exception{
				Map<String, Object> mapaResult = new HashMap<String, Object>();
				try {
					if (log.isDebugEnabled()) log.debug("Inicio - ArticuloController.showView");	
					
					//TODO LISTAR CARGOS 		
					List<ArticuloBean> lstArticulos = articuloService.selArticulos();
					
					mapaResult.put("status", true);
					mapaResult.put("message", lstArticulos.size() + " Registros Encontrados");
					mapaResult.put("data", lstArticulos);
					
				} catch (Exception ex) {			
					if (log.isDebugEnabled()) log.debug("Error - ArticuloController.showView");
					log.error(ex, ex);
					mapaResult.put("status", false);
					mapaResult.put("message", ex.getLocalizedMessage());
				}finally {
					if (log.isDebugEnabled()) log.debug("Fin - ArticuloController.showView");
				}
				
				return mapaResult;
			}
			
	
			
			//TODO METHOD GET CARGO BY ID
			@RequestMapping(value="/Rest/{codigo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
			public @ResponseBody Map<String, Object> getArticulo(@PathVariable String codigo) {	
				Map<String, Object> mapaResult = new HashMap<String, Object>();
				 
				try {
					if (log.isDebugEnabled()) log.debug("Inicio - ArticuloController.getArticulo");	
					
					//TODO LISTAR CARGOS 		
					ArticuloBean articulo =articuloService.selArticulo(codigo);
					if(articulo != null) {
						mapaResult.put("status", true);
						mapaResult.put("message", "1 Registros Encontrados");
						mapaResult.put("data", articulo);
					}else {
						mapaResult.put("status", false);
						mapaResult.put("message", "0 Registros Encontrados");
					}
					
				} catch (Exception ex) {			
					if (log.isDebugEnabled()) log.debug("Error - ArticuloController.getArticulo");
					log.error(ex, ex);
					mapaResult.put("status", false);
					mapaResult.put("message", ex.getLocalizedMessage());
				}finally {
					if (log.isDebugEnabled()) log.debug("Fin - ArticuloController.getArticulo");
				}
				
				return mapaResult;
			}
			
			
			//TODO METHOD POST
					@RequestMapping(value="/Rest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
					public @ResponseBody Map<String, Object> postArticulo(HttpServletRequest request, HttpServletResponse response) throws Exception {
						Map<String, Object> mapaResult = new HashMap<String, Object>();		
						try {
							if (log.isDebugEnabled()) log.debug("Inicio - ArticuloController.postArticulo");			
							//LECTURA DEL REQUEST BODY JSON
							BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream(),"utf-8"));
							StringBuilder jsonEnviado = new StringBuilder();
							String line;
					        while ((line = reader.readLine()) != null) {
					        	jsonEnviado.append(line).append('\n');
					        }
					        //----------------------------------------------
					        //TODO SERIALIZAMOS EL JSON -> CLASS 
							ArticuloBean articuloNuevo = (ArticuloBean) new JsonSerializer().deserialize(jsonEnviado.toString(), ArticuloBean.class);			
							//----------------------------------------------
							//TODO SERVICE INSERTAR USUARIO
							result = articuloService.insArticulo(articuloNuevo);
							//----------------------------------------------
							if(result == true) {
								mapaResult.put("status", true);
								mapaResult.put("message", "1 Registro Guardado");
							}else {
								mapaResult.put("status", false);
								mapaResult.put("message", "0 Registro Guardado");
							}			
						} catch (Exception ex) {			
							if (log.isDebugEnabled()) log.debug("Error - ArticuloController.postArticulo");
							log.error(ex, ex);
							mapaResult.put("status", false);
							mapaResult.put("message", ex.getLocalizedMessage());
						}finally {
							if (log.isDebugEnabled()) log.debug("Fin - ArticuloController.postArticulo");			
						}			
						return mapaResult;
					}
					
					
			//TODO METHOD PUT
					@RequestMapping(value="/Rest", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
					public @ResponseBody Map<String, Object> putArticulo(HttpServletRequest request, HttpServletResponse response) throws Exception  {
						Map<String, Object> mapaResult = new HashMap<String, Object>();		
						try {
							if (log.isDebugEnabled()) log.debug("Inicio - ArticuloController.putArticulo");			
							//LECTURA DEL REQUEST BODY
							BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream(),"utf-8"));
							StringBuilder jsonEnviado = new StringBuilder();
							String line;
					        while ((line = reader.readLine()) != null) {
					        	jsonEnviado.append(line).append('\n');
					        }
							//Map<String, Object> dataEnvio = (Map<String, Object>) new JsonSerializer().deserialize(jsonEnviado, Map.class);
							ArticuloBean articuloNuevo = (ArticuloBean) new JsonSerializer().deserialize(jsonEnviado.toString(), ArticuloBean.class);			
							
							//TODO ACTUALIZAR USUARIO
							result = articuloService.updArticulo(articuloNuevo);
							
							if(result == true) {
								mapaResult.put("status", true);
								mapaResult.put("message", "1 Registro Actualizado");
							}else {
								mapaResult.put("status", false);
								mapaResult.put("message", "0 Registro Actualizado");
							}
							
						} catch (Exception ex) {			
							if (log.isDebugEnabled()) log.debug("Error - ArticuloController.putArticulo");
							log.error(ex, ex);
							mapaResult.put("status", false);
							mapaResult.put("message", ex.getLocalizedMessage());
						}finally {
							if (log.isDebugEnabled()) log.debug("Fin - ArticuloController.putArticulo");
							
						}			
						return mapaResult;
					}
					
					//TODO METHOD DELETE
					@RequestMapping(value="/Rest/{codigo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
						public @ResponseBody Map<String, Object> delArticulo(@PathVariable String codigo) throws Exception {
							Map<String, Object> mapaResult = new HashMap<String, Object>();		
							try {
								if (log.isDebugEnabled()) log.debug("Inicio - ArticuloController.delArticulo");			
								//TODO ELIMINAR USUARIO
								result = articuloService.delArticulo(codigo);
								
								if(result == true) {
									mapaResult.put("status", true);
									mapaResult.put("message", "1 Registros Eliminado");
								}else {
									mapaResult.put("status", false);
									mapaResult.put("message", "0 Registros Eliminado");
								}			
							} catch (Exception ex) {			
								if (log.isDebugEnabled()) log.debug("Error - ArticuloController.delArticulo");
								log.error(ex, ex);
								mapaResult.put("status", false);
								mapaResult.put("message", ex.getLocalizedMessage());
							}finally {
								if (log.isDebugEnabled()) log.debug("Fin - ArticuloController.delArticulo");
							}		
							return mapaResult;
					}
		

}
