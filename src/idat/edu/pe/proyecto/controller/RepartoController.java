package idat.edu.pe.proyecto.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import idat.edu.pe.proyecto.bean.RepartoBean;
import idat.edu.pe.proyecto.service.RepartoService;
import net.sf.sojo.interchange.json.JsonSerializer;

@Controller
@RequestMapping("Reparto")
public class RepartoController {
	
	
	static Log log = LogFactory.getLog(RepartoController.class.getName());
	@Autowired		
	RepartoService repartoService;
	boolean result;
	
	@RequestMapping(value="/Web", method = RequestMethod.GET)
	public ModelAndView showView(HttpServletRequest request, HttpServletResponse response) {
		log.info("RepartoController.showView - Iniciando.");
		ModelAndView modelo = null;
		//HttpSession session = request.getSession(true);
		try {
			//TODO ModalAndView
			modelo = new ModelAndView();
			modelo.setViewName("viewReparto");
			
			Map<String, Object> titulos = new HashMap<String, Object>();		
			titulos.put("tituloDefecto", "Mantenimiento Reparto");			
			
			modelo.addObject("titulos", new JsonSerializer().serialize(titulos));
			
			//TODO LISTAR CARGOS 		
			List<RepartoBean> lstRepartos = repartoService.listarRepartos();		
						
			modelo.addObject("listadoRepartos", new JsonSerializer().serialize(lstRepartos));			
					
		} catch (Exception e) {
			log.error("RepartoController.showView - Error");
			log.error(e.getMessage());
		}
		log.info("RepartoController.showView - Finalizando");
		return modelo;
	}
	
	
	//TODO METHOD GET CARGO ALL	
	@RequestMapping(value="/Rest", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public  @ResponseBody Map<String, Object> getRepartoAll(HttpServletRequest request, HttpServletResponse response) throws Exception{
		Map<String, Object> mapaResult = new HashMap<String, Object>();
		try {
			if (log.isDebugEnabled()) log.debug("Inicio - RepartoController.showView");	
			
			//TODO LISTAR CARGOS 		
			List<RepartoBean> lstRepartos = repartoService.listarRepartos();
			
			mapaResult.put("status", true);
			mapaResult.put("message", lstRepartos.size() + " Registros Encontrados");
			mapaResult.put("data", lstRepartos);
			
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - RepartoController.showView");
			log.error(ex, ex);
			mapaResult.put("status", false);
			mapaResult.put("message", ex.getLocalizedMessage());
		}finally {
			if (log.isDebugEnabled()) log.debug("Fin - RepartoController.showView");
		}
		
		return mapaResult;
	}
	

	//TODO METHOD GET CARGO BY ID
	@RequestMapping(value="/Rest/{codigo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> getReparto(@PathVariable String codigo) {	
		Map<String, Object> mapaResult = new HashMap<String, Object>();
		 
		try {
			if (log.isDebugEnabled()) log.debug("Inicio - RepartoController.getReparto");	
			
			//TODO LISTAR CARGOS 		
			RepartoBean reparto =repartoService.listarReparto(codigo);
			if(reparto != null) {
				mapaResult.put("status", true);
				mapaResult.put("message", "1 Registros Encontrados");
				mapaResult.put("data", reparto);
			}else {
				mapaResult.put("status", false);
				mapaResult.put("message", "0 Registros Encontrados");
			}
			
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - RepartoController.getReparto");
			log.error(ex, ex);
			mapaResult.put("status", false);
			mapaResult.put("message", ex.getLocalizedMessage());
		}finally {
			if (log.isDebugEnabled()) log.debug("Fin - RepartoController.getReparto");
		}
		
		return mapaResult;
	}
	
	//TODO METHOD POST
	@RequestMapping(value="/Rest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> postReparto(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> mapaResult = new HashMap<String, Object>();		
		try {
			if (log.isDebugEnabled()) log.debug("Inicio - RepartoController.postReparto");			
			//LECTURA DEL REQUEST BODY JSON
			BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream(),"utf-8"));
			StringBuilder jsonEnviado = new StringBuilder();
			String line;
	        while ((line = reader.readLine()) != null) {
	        	jsonEnviado.append(line).append('\n');
	        }
	        //----------------------------------------------
	        //TODO SERIALIZAMOS EL JSON -> CLASS 
			RepartoBean repartoNuevo = (RepartoBean) new JsonSerializer().deserialize(jsonEnviado.toString(), RepartoBean.class);			
			//----------------------------------------------
			//TODO SERVICE INSERTAR USUARIO
			result = repartoService.insertarReparto(repartoNuevo);
			//----------------------------------------------
			if(result == true) {
				mapaResult.put("status", true);
				mapaResult.put("message", "1 Registro Guardado");
			}else {
				mapaResult.put("status", false);
				mapaResult.put("message", "0 Registro Guardado");
			}			
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - RepartoController.postReparto");
			log.error(ex, ex);
			mapaResult.put("status", false);
			mapaResult.put("message", ex.getLocalizedMessage());
		}finally {
			if (log.isDebugEnabled()) log.debug("Fin - RepartoController.postReparto");			
		}			
		return mapaResult;
	}
	
	
//TODO METHOD PUT
	@RequestMapping(value="/Rest", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> putReparto(HttpServletRequest request, HttpServletResponse response) throws Exception  {
		Map<String, Object> mapaResult = new HashMap<String, Object>();		
		try {
			if (log.isDebugEnabled()) log.debug("Inicio - RepartoController.putReparto");			
			//LECTURA DEL REQUEST BODY
			BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream(),"utf-8"));
			StringBuilder jsonEnviado = new StringBuilder();
			String line;
	        while ((line = reader.readLine()) != null) {
	        	jsonEnviado.append(line).append('\n');
	        }
			//Map<String, Object> dataEnvio = (Map<String, Object>) new JsonSerializer().deserialize(jsonEnviado, Map.class);
			RepartoBean repartoNuevo = (RepartoBean) new JsonSerializer().deserialize(jsonEnviado.toString(), RepartoBean.class);			
			
			//TODO ACTUALIZAR USUARIO
			result = repartoService.actualizarReparto(repartoNuevo);
			
			if(result == true) {
				mapaResult.put("status", true);
				mapaResult.put("message", "1 Registro Actualizado");
			}else {
				mapaResult.put("status", false);
				mapaResult.put("message", "0 Registro Actualizado");
			}
			
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - RepartoController.putReparto");
			log.error(ex, ex);
			mapaResult.put("status", false);
			mapaResult.put("message", ex.getLocalizedMessage());
		}finally {
			if (log.isDebugEnabled()) log.debug("Fin - RepartoController.putReparto");
			
		}			
		return mapaResult;
	}
	
	//TODO METHOD DELETE
	@RequestMapping(value="/Rest/{codigo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
		public @ResponseBody Map<String, Object> delReparto(@PathVariable String codigo) throws Exception {
			Map<String, Object> mapaResult = new HashMap<String, Object>();		
			try {
				if (log.isDebugEnabled()) log.debug("Inicio - RepartoController.delReparto");			
				//TODO ELIMINAR USUARIO
				result = repartoService.eliminarReparto(codigo);
				
				if(result == true) {
					mapaResult.put("status", true);
					mapaResult.put("message", "1 Registros Eliminado");
				}else {
					mapaResult.put("status", false);
					mapaResult.put("message", "0 Registros Eliminado");
				}			
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - RepartoController.delReparto");
				log.error(ex, ex);
				mapaResult.put("status", false);
				mapaResult.put("message", ex.getLocalizedMessage());
			}finally {
				if (log.isDebugEnabled()) log.debug("Fin - RepartoController.delReparto");
			}		
			return mapaResult;
	}
	
	//TODO METHOD GET CARGO ALL	
		@RequestMapping(value="/Rest/Ruta", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
		public  @ResponseBody Map<String, Object> getRutaAll(HttpServletRequest request, HttpServletResponse response) throws Exception{
			Map<String, Object> mapaResult = new HashMap<String, Object>();
			try {
				if (log.isDebugEnabled()) log.debug("Inicio - RepartoController.showView");	
				
				//TODO LISTAR CARGOS 		
				List<RepartoBean> lstRepartos = repartoService.listarRutas();
				
				mapaResult.put("status", true);
				mapaResult.put("message", lstRepartos.size() + " Registros Encontrados");
				mapaResult.put("data", lstRepartos);
				
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - RepartoController.showView");
				log.error(ex, ex);
				mapaResult.put("status", false);
				mapaResult.put("message", ex.getLocalizedMessage());
			}finally {
				if (log.isDebugEnabled()) log.debug("Fin - RepartoController.showView");
			}
			
			return mapaResult;
		}


}
