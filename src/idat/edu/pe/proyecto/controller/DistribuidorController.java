package idat.edu.pe.proyecto.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import idat.edu.pe.proyecto.bean.DistribuidorBean;

import idat.edu.pe.proyecto.service.DistribuidorService;
import net.sf.sojo.interchange.json.JsonSerializer;

@Controller
@RequestMapping("Distribuidor")
public class DistribuidorController {
	
	static Log log = LogFactory.getLog(DistribuidorController.class.getName());
	@Autowired		
	DistribuidorService distribuidorService;
	boolean result;
	
	
	@RequestMapping(value="/Web", method = RequestMethod.GET)
	public ModelAndView showView(HttpServletRequest request, HttpServletResponse response) {
		log.info("DistribuidorController.showView - Iniciando.");
		ModelAndView modelo = null;
		//HttpSession session = request.getSession(true);
		try {
			//TODO ModalAndView
			modelo = new ModelAndView();
			modelo.setViewName("viewDistribuidor");
			
			Map<String, Object> titulos = new HashMap<String, Object>();		
			titulos.put("tituloDefecto", "Mantenimiento Distribuidor");			
			
			modelo.addObject("titulos", new JsonSerializer().serialize(titulos));
			
			//TODO LISTAR CARGOS 		
			List<DistribuidorBean> lstDistribuidors = distribuidorService.listarDistribuidors();		
						
			modelo.addObject("listadoDistribuidors", new JsonSerializer().serialize(lstDistribuidors));			
					
		} catch (Exception e) {
			log.error("DistribuidorController.showView - Error");
			log.error(e.getMessage());
		}
		log.info("DistribuidorController.showView - Finalizando");
		return modelo;
	}
	

	//TODO METHOD GET CARGO ALL	
			@RequestMapping(value="/Rest", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
			public  @ResponseBody Map<String, Object> getDistribuidorAll(HttpServletRequest request, HttpServletResponse response) throws Exception{
				Map<String, Object> mapaResult = new HashMap<String, Object>();
				try {
					if (log.isDebugEnabled()) log.debug("Inicio - DistribuidorController.showView");	
					
					//TODO LISTAR CARGOS 		
					List<DistribuidorBean> lstDistribuidors = distribuidorService.listarDistribuidors();
					
					mapaResult.put("status", true);
					mapaResult.put("message", lstDistribuidors.size() + " Registros Encontrados");
					mapaResult.put("data", lstDistribuidors);
					
				} catch (Exception ex) {			
					if (log.isDebugEnabled()) log.debug("Error - DistribuidorController.showView");
					log.error(ex, ex);
					mapaResult.put("status", false);
					mapaResult.put("message", ex.getLocalizedMessage());
				}finally {
					if (log.isDebugEnabled()) log.debug("Fin - DistribuidorController.showView");
				}
				
				return mapaResult;
			}
			
			//TODO METHOD GET CARGO BY ID
			@RequestMapping(value="/Rest/{codigo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
			public @ResponseBody Map<String, Object> getDistribuidor(@PathVariable String codigo) {	
				Map<String, Object> mapaResult = new HashMap<String, Object>();
				 
				try {
					if (log.isDebugEnabled()) log.debug("Inicio - DistribuidorController.getDistribuidor");	
					
					//TODO LISTAR CARGOS 		
					DistribuidorBean distribuidor =distribuidorService.listarDistribuidor(codigo);
					if(distribuidor != null) {
						mapaResult.put("status", true);
						mapaResult.put("message", "1 Registros Encontrados");
						mapaResult.put("data", distribuidor);
					}else {
						mapaResult.put("status", false);
						mapaResult.put("message", "0 Registros Encontrados");
					}
					
				} catch (Exception ex) {			
					if (log.isDebugEnabled()) log.debug("Error - DistribuidorController.getDistribuidor");
					log.error(ex, ex);
					mapaResult.put("status", false);
					mapaResult.put("message", ex.getLocalizedMessage());
				}finally {
					if (log.isDebugEnabled()) log.debug("Fin - DistribuidorController.getDistribuidor");
				}
				
				return mapaResult;
			}
			
			
			//TODO METHOD POST
			@RequestMapping(value="/Rest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
			public @ResponseBody Map<String, Object> postDistribuidor(HttpServletRequest request, HttpServletResponse response) throws Exception {
				Map<String, Object> mapaResult = new HashMap<String, Object>();		
				try {
					if (log.isDebugEnabled()) log.debug("Inicio - DistribuidorController.postDistribuidor");			
					//LECTURA DEL REQUEST BODY JSON
					BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream(),"utf-8"));
					StringBuilder jsonEnviado = new StringBuilder();
					String line;
			        while ((line = reader.readLine()) != null) {
			        	jsonEnviado.append(line).append('\n');
			        }
			        //----------------------------------------------
			        //TODO SERIALIZAMOS EL JSON -> CLASS 
					DistribuidorBean distribuidorNuevo = (DistribuidorBean) new JsonSerializer().deserialize(jsonEnviado.toString(), DistribuidorBean.class);			
					//----------------------------------------------
					//TODO SERVICE INSERTAR USUARIO
					result = distribuidorService.insertarDistribuidor(distribuidorNuevo);
					//----------------------------------------------
					if(result == true) {
						mapaResult.put("status", true);
						mapaResult.put("message", "1 Registro Guardado");
					}else {
						mapaResult.put("status", false);
						mapaResult.put("message", "0 Registro Guardado");
					}			
				} catch (Exception ex) {			
					if (log.isDebugEnabled()) log.debug("Error - DistribuidorController.postDistribuidor");
					log.error(ex, ex);
					mapaResult.put("status", false);
					mapaResult.put("message", ex.getLocalizedMessage());
				}finally {
					if (log.isDebugEnabled()) log.debug("Fin - DistribuidorController.postDistribuidor");			
				}			
				return mapaResult;
			}
					
			//TODO METHOD PUT
			@RequestMapping(value="/Rest", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
			public @ResponseBody Map<String, Object> putDistribuidor(HttpServletRequest request, HttpServletResponse response) throws Exception  {
				Map<String, Object> mapaResult = new HashMap<String, Object>();		
				try {
					if (log.isDebugEnabled()) log.debug("Inicio - DistribuidorController.putDistribuidor");			
					//LECTURA DEL REQUEST BODY
					BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream(),"utf-8"));
					StringBuilder jsonEnviado = new StringBuilder();
					String line;
			        while ((line = reader.readLine()) != null) {
			        	jsonEnviado.append(line).append('\n');
			        }
					//Map<String, Object> dataEnvio = (Map<String, Object>) new JsonSerializer().deserialize(jsonEnviado, Map.class);
					DistribuidorBean distribuidorNuevo = (DistribuidorBean) new JsonSerializer().deserialize(jsonEnviado.toString(), DistribuidorBean.class);			
					
					//TODO ACTUALIZAR USUARIO
					result = distribuidorService.actualizarDistribuidor(distribuidorNuevo);
					
					if(result == true) {
						mapaResult.put("status", true);
						mapaResult.put("message", "1 Registro Actualizado");
					}else {
						mapaResult.put("status", false);
						mapaResult.put("message", "0 Registro Actualizado");
					}
					
				} catch (Exception ex) {			
					if (log.isDebugEnabled()) log.debug("Error - DistribuidorController.putDistribuidor");
					log.error(ex, ex);
					mapaResult.put("status", false);
					mapaResult.put("message", ex.getLocalizedMessage());
				}finally {
					if (log.isDebugEnabled()) log.debug("Fin - DistribuidorController.putDistribuidor");
					
				}			
				return mapaResult;
			}
			
			//TODO METHOD DELETE
			@RequestMapping(value="/Rest/{codigo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
				public @ResponseBody Map<String, Object> delDistribuidor(@PathVariable String codigo) throws Exception {
					Map<String, Object> mapaResult = new HashMap<String, Object>();		
					try {
						if (log.isDebugEnabled()) log.debug("Inicio - DistribuidorController.delDistribuidor");			
						//TODO ELIMINAR USUARIO
						result = distribuidorService.eliminarDistribuidor(codigo);
						
						if(result == true) {
							mapaResult.put("status", true);
							mapaResult.put("message", "1 Registros Eliminado");
						}else {
							mapaResult.put("status", false);
							mapaResult.put("message", "0 Registros Eliminado");
						}			
					} catch (Exception ex) {			
						if (log.isDebugEnabled()) log.debug("Error - DistribuidorController.delDistribuidor");
						log.error(ex, ex);
						mapaResult.put("status", false);
						mapaResult.put("message", ex.getLocalizedMessage());
					}finally {
						if (log.isDebugEnabled()) log.debug("Fin - DistribuidorController.delDistribuidor");
					}		
					return mapaResult;
			}
			
			//TODO METHOD GET LOGIN BY USR_EMAIL AND USR_PASSWORD
			@RequestMapping(value="/Rest/{usuario}/{contra}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
			public @ResponseBody Map<String, Object> getDistribuidor(@PathVariable String usuario,@PathVariable String contra) {	
				Map<String, Object> mapaResult = new HashMap<String, Object>();
				 
				try {
					if (log.isDebugEnabled()) log.debug("Inicio - DistribuidorController.getDistribuidor");	
					
					//TODO LISTAR CARGOS 		
					DistribuidorBean distribuidor =distribuidorService.loginDistribuidor(usuario,contra);
					if(distribuidor != null) {
						mapaResult.put("status", true);
						mapaResult.put("message", "1 Registros Encontrados");
						mapaResult.put("data", distribuidor);
					}else {
						mapaResult.put("status", false);
						mapaResult.put("message", "0 Registros Encontrados");
					}
					
				} catch (Exception ex) {			
					if (log.isDebugEnabled()) log.debug("Error - DistribuidorController.getDistribuidor");
					log.error(ex, ex);
					mapaResult.put("status", false);
					mapaResult.put("message", ex.getLocalizedMessage());
				}finally {
					if (log.isDebugEnabled()) log.debug("Fin - DistribuidorController.getDistribuidor");
				}
				
				return mapaResult;
			}


			
			
}
