package idat.edu.pe.proyecto.controller;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import idat.edu.pe.proyecto.bean.ClienteBean;
import idat.edu.pe.proyecto.service.ClienteService;
import net.sf.sojo.interchange.json.JsonSerializer;

@Controller
@RequestMapping("Cliente")

public class ClienteController {
	
	static Log log = LogFactory.getLog(ClienteController.class.getName());
	@Autowired		
	ClienteService clienteService;
	boolean result;
	
	@RequestMapping(value="/Web", method = RequestMethod.GET)
	public ModelAndView showView(HttpServletRequest request, HttpServletResponse response) {
		log.info("ClienteController.showView - Iniciando.");
		ModelAndView modelo = null;
		//HttpSession session = request.getSession(true);
		try {
			//TODO ModalAndView
			modelo = new ModelAndView();
			modelo.setViewName("viewCliente");
			
			Map<String, Object> titulos = new HashMap<String, Object>();		
			titulos.put("tituloDefecto", "Mantenimiento Cliente");			
			
			modelo.addObject("titulos", new JsonSerializer().serialize(titulos));
			
			//TODO LISTAR CARGOS 		
			List<ClienteBean> lstClientes = clienteService.listarClientes();		
						
			modelo.addObject("listadoClientes", new JsonSerializer().serialize(lstClientes));			
					
		} catch (Exception e) {
			log.error("ClienteController.showView - Error");
			log.error(e.getMessage());
		}
		log.info("ClienteController.showView - Finalizando");
		return modelo;
	}
	
	
	//TODO METHOD GET CARGO ALL	
			@RequestMapping(value="/Rest", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
			public  @ResponseBody Map<String, Object> getClienteAll(HttpServletRequest request, HttpServletResponse response) throws Exception{
				Map<String, Object> mapaResult = new HashMap<String, Object>();
				try {
					if (log.isDebugEnabled()) log.debug("Inicio - ClienteController.showView");	
					
					//TODO LISTAR CARGOS 		
					List<ClienteBean> lstClientes = clienteService.listarClientes();
					
					mapaResult.put("status", true);
					mapaResult.put("message", lstClientes.size() + " Registros Encontrados");
					mapaResult.put("data", lstClientes);
					
				} catch (Exception ex) {			
					if (log.isDebugEnabled()) log.debug("Error - ClienteController.showView");
					log.error(ex, ex);
					mapaResult.put("status", false);
					mapaResult.put("message", ex.getLocalizedMessage());
				}finally {
					if (log.isDebugEnabled()) log.debug("Fin - ClienteController.showView");
				}
				
				return mapaResult;
			}
			
	
			
			//TODO METHOD GET CARGO BY ID
			@RequestMapping(value="/Rest/{codigo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
			public @ResponseBody Map<String, Object> getCliente(@PathVariable String codigo) {	
				Map<String, Object> mapaResult = new HashMap<String, Object>();
				 
				try {
					if (log.isDebugEnabled()) log.debug("Inicio - ClienteController.getCliente");	
					
					//TODO LISTAR CARGOS 		
					ClienteBean cliente =clienteService.listarCliente(codigo);
					if(cliente != null) {
						mapaResult.put("status", true);
						mapaResult.put("message", "1 Registros Encontrados");
						mapaResult.put("data", cliente);
					}else {
						mapaResult.put("status", false);
						mapaResult.put("message", "0 Registros Encontrados");
					}
					
				} catch (Exception ex) {			
					if (log.isDebugEnabled()) log.debug("Error - ClienteController.getCliente");
					log.error(ex, ex);
					mapaResult.put("status", false);
					mapaResult.put("message", ex.getLocalizedMessage());
				}finally {
					if (log.isDebugEnabled()) log.debug("Fin - ClienteController.getCliente");
				}
				
				return mapaResult;
			}
			
			
			//TODO METHOD POST
					@RequestMapping(value="/Rest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
					public @ResponseBody Map<String, Object> postCliente(HttpServletRequest request, HttpServletResponse response) throws Exception {
						Map<String, Object> mapaResult = new HashMap<String, Object>();		
						try {
							if (log.isDebugEnabled()) log.debug("Inicio - ClienteController.postCliente");			
							//LECTURA DEL REQUEST BODY JSON
							BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream(),"utf-8"));
							StringBuilder jsonEnviado = new StringBuilder();
							String line;
					        while ((line = reader.readLine()) != null) {
					        	jsonEnviado.append(line).append('\n');
					        }
					        //----------------------------------------------
					        //TODO SERIALIZAMOS EL JSON -> CLASS 
							ClienteBean clienteNuevo = (ClienteBean) new JsonSerializer().deserialize(jsonEnviado.toString(), ClienteBean.class);			
							//----------------------------------------------
							//TODO SERVICE INSERTAR USUARIO
							result = clienteService.insertarCliente(clienteNuevo);
							//----------------------------------------------
							if(result == true) {
								mapaResult.put("status", true);
								mapaResult.put("message", "1 Registro Guardado");
							}else {
								mapaResult.put("status", false);
								mapaResult.put("message", "0 Registro Guardado");
							}			
						} catch (Exception ex) {			
							if (log.isDebugEnabled()) log.debug("Error - ClienteController.postCliente");
							log.error(ex, ex);
							mapaResult.put("status", false);
							mapaResult.put("message", ex.getLocalizedMessage());
						}finally {
							if (log.isDebugEnabled()) log.debug("Fin - ClienteController.postCliente");			
						}			
						return mapaResult;
					}
					
					
			//TODO METHOD PUT
					@RequestMapping(value="/Rest", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
					public @ResponseBody Map<String, Object> putCliente(HttpServletRequest request, HttpServletResponse response) throws Exception  {
						Map<String, Object> mapaResult = new HashMap<String, Object>();		
						try {
							if (log.isDebugEnabled()) log.debug("Inicio - ClienteController.putCliente");			
							//LECTURA DEL REQUEST BODY
							BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream(),"utf-8"));
							StringBuilder jsonEnviado = new StringBuilder();
							String line;
					        while ((line = reader.readLine()) != null) {
					        	jsonEnviado.append(line).append('\n');
					        }
							//Map<String, Object> dataEnvio = (Map<String, Object>) new JsonSerializer().deserialize(jsonEnviado, Map.class);
							ClienteBean clienteNuevo = (ClienteBean) new JsonSerializer().deserialize(jsonEnviado.toString(), ClienteBean.class);			
							
							//TODO ACTUALIZAR USUARIO
							result = clienteService.actualizarCliente(clienteNuevo);
							
							if(result == true) {
								mapaResult.put("status", true);
								mapaResult.put("message", "1 Registro Actualizado");
							}else {
								mapaResult.put("status", false);
								mapaResult.put("message", "0 Registro Actualizado");
							}
							
						} catch (Exception ex) {			
							if (log.isDebugEnabled()) log.debug("Error - ClienteController.putCliente");
							log.error(ex, ex);
							mapaResult.put("status", false);
							mapaResult.put("message", ex.getLocalizedMessage());
						}finally {
							if (log.isDebugEnabled()) log.debug("Fin - ClienteController.putCliente");
							
						}			
						return mapaResult;
					}
					
					//TODO METHOD DELETE
					@RequestMapping(value="/Rest/{codigo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
						public @ResponseBody Map<String, Object> delCliente(@PathVariable String codigo) throws Exception {
							Map<String, Object> mapaResult = new HashMap<String, Object>();		
							try {
								if (log.isDebugEnabled()) log.debug("Inicio - ClienteController.delCliente");			
								//TODO ELIMINAR USUARIO
								result = clienteService.eliminarCliente(codigo);
								
								if(result == true) {
									mapaResult.put("status", true);
									mapaResult.put("message", "1 Registros Eliminado");
								}else {
									mapaResult.put("status", false);
									mapaResult.put("message", "0 Registros Eliminado");
								}			
							} catch (Exception ex) {			
								if (log.isDebugEnabled()) log.debug("Error - ClienteController.delCliente");
								log.error(ex, ex);
								mapaResult.put("status", false);
								mapaResult.put("message", ex.getLocalizedMessage());
							}finally {
								if (log.isDebugEnabled()) log.debug("Fin - ClienteController.delCliente");
							}		
							return mapaResult;
					}
		

	

}
