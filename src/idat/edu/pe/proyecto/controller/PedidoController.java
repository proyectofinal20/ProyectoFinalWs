package idat.edu.pe.proyecto.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import idat.edu.pe.proyecto.bean.PedidoBean;

import idat.edu.pe.proyecto.service.PedidoService;
import net.sf.sojo.interchange.json.JsonSerializer;

@Controller
@RequestMapping("Pedido")
public class PedidoController {
	

	static Log log = LogFactory.getLog(PedidoController.class.getName());
	@Autowired		
	PedidoService pedidoService;
	boolean result;
	
	@RequestMapping(value="/Web", method = RequestMethod.GET)
	public ModelAndView showView(HttpServletRequest request, HttpServletResponse response) {
		log.info("PedidoController.showView - Iniciando.");
		ModelAndView modelo = null;
		//HttpSession session = request.getSession(true);
		try {
			//TODO ModalAndView
			modelo = new ModelAndView();
			modelo.setViewName("viewPedido");
			
			Map<String, Object> titulos = new HashMap<String, Object>();		
			titulos.put("tituloDefecto", "Mantenimiento Pedido");			
			
			modelo.addObject("titulos", new JsonSerializer().serialize(titulos));
			
			//TODO LISTAR CARGOS 		
			List<PedidoBean> lstPedidos = pedidoService.listarPedidos();		
						
			modelo.addObject("listadoPedidos", new JsonSerializer().serialize(lstPedidos));			
					
		} catch (Exception e) {
			log.error("PedidoController.showView - Error");
			log.error(e.getMessage());
		}
		log.info("PedidoController.showView - Finalizando");
		return modelo;
	}
	
	
	//TODO METHOD GET CARGO ALL	
	@RequestMapping(value="/Rest", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public  @ResponseBody Map<String, Object> getPedidoAll(HttpServletRequest request, HttpServletResponse response) throws Exception{
		Map<String, Object> mapaResult = new HashMap<String, Object>();
		try {
			if (log.isDebugEnabled()) log.debug("Inicio - PedidoController.showView");	
			
			//TODO LISTAR CARGOS 		
			List<PedidoBean> lstPedidos = pedidoService.listarPedidos();
			
			mapaResult.put("status", true);
			mapaResult.put("message", lstPedidos.size() + " Registros Encontrados");
			mapaResult.put("data", lstPedidos);
			
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - PedidoController.showView");
			log.error(ex, ex);
			mapaResult.put("status", false);
			mapaResult.put("message", ex.getLocalizedMessage());
		}finally {
			if (log.isDebugEnabled()) log.debug("Fin - PedidoController.showView");
		}
		
		return mapaResult;
	}
	

	//TODO METHOD GET CARGO BY ID
	@RequestMapping(value="/Rest/{codigo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> getPedido(@PathVariable String codigo) {	
		Map<String, Object> mapaResult = new HashMap<String, Object>();
		 
		try {
			if (log.isDebugEnabled()) log.debug("Inicio - PedidoController.getPedido");	
			
			//TODO LISTAR CARGOS 		
			PedidoBean pedido =pedidoService.listarPedido(codigo);
			if(pedido != null) {
				mapaResult.put("status", true);
				mapaResult.put("message", "1 Registros Encontrados");
				mapaResult.put("data", pedido);
			}else {
				mapaResult.put("status", false);
				mapaResult.put("message", "0 Registros Encontrados");
			}
			
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - PedidoController.getPedido");
			log.error(ex, ex);
			mapaResult.put("status", false);
			mapaResult.put("message", ex.getLocalizedMessage());
		}finally {
			if (log.isDebugEnabled()) log.debug("Fin - PedidoController.getPedido");
		}
		
		return mapaResult;
	}
	
	//TODO METHOD POST
	@RequestMapping(value="/Rest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> postPedido(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> mapaResult = new HashMap<String, Object>();		
		try {
			if (log.isDebugEnabled()) log.debug("Inicio - PedidoController.postPedido");			
			//LECTURA DEL REQUEST BODY JSON
			BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream(),"utf-8"));
			StringBuilder jsonEnviado = new StringBuilder();
			String line;
	        while ((line = reader.readLine()) != null) {
	        	jsonEnviado.append(line).append('\n');
	        }
	        //----------------------------------------------
	        //TODO SERIALIZAMOS EL JSON -> CLASS 
			PedidoBean pedidoNuevo = (PedidoBean) new JsonSerializer().deserialize(jsonEnviado.toString(), PedidoBean.class);			
			//----------------------------------------------
			//TODO SERVICE INSERTAR USUARIO
			result = pedidoService.insertarPedido(pedidoNuevo);
			//----------------------------------------------
			if(result == true) {
				mapaResult.put("status", true);
				mapaResult.put("message", "1 Registro Guardado");
			}else {
				mapaResult.put("status", false);
				mapaResult.put("message", "0 Registro Guardado");
			}			
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - PedidoController.postPedido");
			log.error(ex, ex);
			mapaResult.put("status", false);
			mapaResult.put("message", ex.getLocalizedMessage());
		}finally {
			if (log.isDebugEnabled()) log.debug("Fin - PedidoController.postPedido");			
		}			
		return mapaResult;
	}
	
	
//TODO METHOD PUT
	@RequestMapping(value="/Rest", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> putPedido(HttpServletRequest request, HttpServletResponse response) throws Exception  {
		Map<String, Object> mapaResult = new HashMap<String, Object>();		
		try {
			if (log.isDebugEnabled()) log.debug("Inicio - PedidoController.putPedido");			
			//LECTURA DEL REQUEST BODY
			BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream(),"utf-8"));
			StringBuilder jsonEnviado = new StringBuilder();
			String line;
	        while ((line = reader.readLine()) != null) {
	        	jsonEnviado.append(line).append('\n');
	        }
			//Map<String, Object> dataEnvio = (Map<String, Object>) new JsonSerializer().deserialize(jsonEnviado, Map.class);
			PedidoBean pedidoNuevo = (PedidoBean) new JsonSerializer().deserialize(jsonEnviado.toString(), PedidoBean.class);			
			
			//TODO ACTUALIZAR USUARIO
			result = pedidoService.actualizarPedido(pedidoNuevo);
			
			if(result == true) {
				mapaResult.put("status", true);
				mapaResult.put("message", "1 Registro Actualizado");
			}else {
				mapaResult.put("status", false);
				mapaResult.put("message", "0 Registro Actualizado");
			}
			
		} catch (Exception ex) {			
			if (log.isDebugEnabled()) log.debug("Error - PedidoController.putPedido");
			log.error(ex, ex);
			mapaResult.put("status", false);
			mapaResult.put("message", ex.getLocalizedMessage());
		}finally {
			if (log.isDebugEnabled()) log.debug("Fin - PedidoController.putPedido");
			
		}			
		return mapaResult;
	}
	
	//TODO METHOD DELETE
	@RequestMapping(value="/Rest/{codigo}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
		public @ResponseBody Map<String, Object> delPedido(@PathVariable String codigo) throws Exception {
			Map<String, Object> mapaResult = new HashMap<String, Object>();		
			try {
				if (log.isDebugEnabled()) log.debug("Inicio - PedidoController.delPedido");			
				//TODO ELIMINAR USUARIO
				result = pedidoService.eliminarPedido(codigo);
				
				if(result == true) {
					mapaResult.put("status", true);
					mapaResult.put("message", "1 Registros Eliminado");
				}else {
					mapaResult.put("status", false);
					mapaResult.put("message", "0 Registros Eliminado");
				}			
			} catch (Exception ex) {			
				if (log.isDebugEnabled()) log.debug("Error - PedidoController.delPedido");
				log.error(ex, ex);
				mapaResult.put("status", false);
				mapaResult.put("message", ex.getLocalizedMessage());
			}finally {
				if (log.isDebugEnabled()) log.debug("Fin - PedidoController.delPedido");
			}		
			return mapaResult;
	}


	
	
	

}
