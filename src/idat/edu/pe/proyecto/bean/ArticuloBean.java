package idat.edu.pe.proyecto.bean;

import idat.edu.pe.proyecto.model.ArticuloModel;

public class ArticuloBean extends ArticuloModel{
	private static final long serialVersionUID = 1L;
	
	String art_desestado;

	public String getArt_desestado() {
		return art_desestado;
	}

	public void setArt_desestado(String art_desestado) {
		this.art_desestado = art_desestado;
	}
	

}
