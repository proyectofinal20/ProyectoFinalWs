package idat.edu.pe.proyecto.bean;

import idat.edu.pe.proyecto.model.RepartoModel;

public class RepartoBean  extends RepartoModel{
	private static final long serialVersionUID = 1L;
	
	String repd_desestado;

	public String getRepd_desestado() {
		return repd_desestado;
	}

	public void setRepd_desestado(String repd_desestado) {
		this.repd_desestado = repd_desestado;
	}
	
	

}
