package idat.edu.pe.proyecto.model;

import java.io.Serializable;

public class RepartoModel implements Serializable{
	private static final long serialVersionUID = 1L;

	String rep_codigo;
	String rep_fecemi;
	String rep_observ;
	String dist_codigo;
	int rep_estado;
	String repd_repcod;
	String repd_pedcod;
	int repd_estado;

	String ruta_fecha;
	String ruta_nrocliente;
	
	
	public String getRuta_fecha() {
		return ruta_fecha;
	}
	public void setRuta_fecha(String ruta_fecha) {
		this.ruta_fecha = ruta_fecha;
	}
	public String getRuta_nrocliente() {
		return ruta_nrocliente;
	}
	public void setRuta_nrocliente(String ruta_nrocliente) {
		this.ruta_nrocliente = ruta_nrocliente;
	}
	
	public String getRep_codigo() {
		return rep_codigo;
	}
	public void setRep_codigo(String rep_codigo) {
		this.rep_codigo = rep_codigo;
	}
	public String getRep_fecemi() {
		return rep_fecemi;
	}
	public void setRep_fecemi(String rep_fecemi) {
		this.rep_fecemi = rep_fecemi;
	}
	public String getRep_observ() {
		return rep_observ;
	}
	public void setRep_observ(String rep_observ) {
		this.rep_observ = rep_observ;
	}
	public String getDist_codigo() {
		return dist_codigo;
	}
	public void setDist_codigo(String dist_codigo) {
		this.dist_codigo = dist_codigo;
	}
	public int getRep_estado() {
		return rep_estado;
	}
	public void setRep_estado(int rep_estado) {
		this.rep_estado = rep_estado;
	}
	public String getRepd_repcod() {
		return repd_repcod;
	}
	public void setRepd_repcod(String repd_repcod) {
		this.repd_repcod = repd_repcod;
	}
	public String getRepd_pedcod() {
		return repd_pedcod;
	}
	public void setRepd_pedcod(String repd_pedcod) {
		this.repd_pedcod = repd_pedcod;
	}
	public int getRepd_estado() {
		return repd_estado;
	}
	public void setRepd_estado(int repd_estado) {
		this.repd_estado = repd_estado;
	}
	
	
	
}
