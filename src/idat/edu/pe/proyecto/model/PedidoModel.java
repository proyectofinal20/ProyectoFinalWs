package idat.edu.pe.proyecto.model;

import java.io.Serializable;

public class PedidoModel implements Serializable {
	private static final long serialVersionUID = 1L;

	String ped_codigo;
	String ped_fecemi;
	String ped_clicod;
	int ped_estado;
	
	
	public String getPed_codigo() {
		return ped_codigo;
	}
	public void setPed_codigo(String ped_codigo) {
		this.ped_codigo = ped_codigo;
	}
	public String getPed_fecemi() {
		return ped_fecemi;
	}
	public void setPed_fecemi(String ped_fecemi) {
		this.ped_fecemi = ped_fecemi;
	}
	public String getPed_clicod() {
		return ped_clicod;
	}
	public void setPed_clicod(String ped_clicod) {
		this.ped_clicod = ped_clicod;
	}
	public int getPed_estado() {
		return ped_estado;
	}
	public void setPed_estado(int ped_estado) {
		this.ped_estado = ped_estado;
	}
		
}
