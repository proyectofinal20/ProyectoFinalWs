package idat.edu.pe.proyecto.model;

import java.io.Serializable;

public class PedidoDetalleModel implements Serializable{
	private static final long serialVersionUID = 1L;
	
	String pedd_codigo;
	String pedd_artcod;
	int pedd_cantidad;
	double  pedd_precio;
	int pedd_estado;
	
	public String getPedd_codigo() {
		return pedd_codigo;
	}
	public void setPedd_codigo(String pedd_codigo) {
		this.pedd_codigo = pedd_codigo;
	}
	public String getPedd_artcod() {
		return pedd_artcod;
	}
	public void setPedd_artcod(String pedd_artcod) {
		this.pedd_artcod = pedd_artcod;
	}
	public int getPedd_cantidad() {
		return pedd_cantidad;
	}
	public void setPedd_cantidad(int pedd_cantidad) {
		this.pedd_cantidad = pedd_cantidad;
	}
	public double getPedd_precio() {
		return pedd_precio;
	}
	public void setPedd_precio(double pedd_precio) {
		this.pedd_precio = pedd_precio;
	}
	public int getPedd_estado() {
		return pedd_estado;
	}
	public void setPedd_estado(int pedd_estado) {
		this.pedd_estado = pedd_estado;
	}

}
