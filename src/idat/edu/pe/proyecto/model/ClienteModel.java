package idat.edu.pe.proyecto.model;
import java.io.Serializable;

public class ClienteModel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	String cli_codigo;
	String cli_apellido;
	String cli_nombre;
	String cli_movil1;
	String cli_movil2;
	String cli_email;
	String cli_direc;
	String cli_ubigeo;
	String cli_imagen;
	String cli_latitud;
	String cli_longitud;
	int cli_estado;
	
	
	
	public String getCli_codigo() {
		return cli_codigo;
	}
	public void setCli_codigo(String cli_codigo) {
		this.cli_codigo = cli_codigo;
	}
	public String getCli_apellido() {
		return cli_apellido;
	}
	public void setCli_apellido(String cli_apellido) {
		this.cli_apellido = cli_apellido;
	}
	public String getCli_nombre() {
		return cli_nombre;
	}
	public void setCli_nombre(String cli_nombre) {
		this.cli_nombre = cli_nombre;
	}
	public String getCli_movil1() {
		return cli_movil1;
	}
	public void setCli_movil1(String cli_movil1) {
		this.cli_movil1 = cli_movil1;
	}
	public String getCli_movil2() {
		return cli_movil2;
	}
	public void setCli_movil2(String cli_movil2) {
		this.cli_movil2 = cli_movil2;
	}
	public String getCli_email() {
		return cli_email;
	}
	public void setCli_email(String cli_email) {
		this.cli_email = cli_email;
	}
	public String getCli_direc() {
		return cli_direc;
	}
	public void setCli_direc(String cli_direc) {
		this.cli_direc = cli_direc;
	}
	public String getCli_ubigeo() {
		return cli_ubigeo;
	}
	public void setCli_ubigeo(String cli_ubigeo) {
		this.cli_ubigeo = cli_ubigeo;
	}
	public String getCli_imagen() {
		return cli_imagen;
	}
	public void setCli_imagen(String cli_imagen) {
		this.cli_imagen = cli_imagen;
	}
	public String getCli_latitud() {
		return cli_latitud;
	}
	public void setCli_latitud(String cli_latitud) {
		this.cli_latitud = cli_latitud;
	}
	public String getCli_longitud() {
		return cli_longitud;
	}
	public void setCli_longitud(String cli_longitud) {
		this.cli_longitud = cli_longitud;
	}
	public int getCli_estado() {
		return cli_estado;
	}
	public void setCli_estado(int cli_estado) {
		this.cli_estado = cli_estado;
	}
	
	
	
	
	
	
}
