package idat.edu.pe.proyecto.model;

import java.io.Serializable;

public class DistribuidorModel implements Serializable{
	private static final long serialVersionUID = 1L;
	
	String dist_codigo;
	String dist_usuario;
	String dist_password;
	int dist_estado;
	
	
	
	public String getDist_codigo() {
		return dist_codigo;
	}
	public void setDist_codigo(String dist_codigo) {
		this.dist_codigo = dist_codigo;
	}
	public String getDist_usuario() {
		return dist_usuario;
	}
	public void setDist_usuario(String dist_usuario) {
		this.dist_usuario = dist_usuario;
	}
	public String getDist_password() {
		return dist_password;
	}
	public void setDist_password(String dist_password) {
		this.dist_password = dist_password;
	}
	public int getDist_estado() {
		return dist_estado;
	}
	public void setDist_estado(int dist_estado) {
		this.dist_estado = dist_estado;
	}
	
	

}
