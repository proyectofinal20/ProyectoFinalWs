package idat.edu.pe.proyecto.model;

import java.io.Serializable;

public class ArticuloModel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	String art_codigo;
	String art_descripcion;
	double art_precunidad;
	int art_estado;
	
	
	public String getArt_codigo() {
		return art_codigo;
	}
	public void setArt_codigo(String art_codigo) {
		this.art_codigo = art_codigo;
	}
	public String getArt_descripcion() {
		return art_descripcion;
	}
	public void setArt_descripcion(String art_descripcion) {
		this.art_descripcion = art_descripcion;
	}
	public double getArt_precunidad() {
		return art_precunidad;
	}
	public void setArt_precunidad(double art_precunidad) {
		this.art_precunidad = art_precunidad;
	}
	public int getArt_estado() {
		return art_estado;
	}
	public void setArt_estado(int art_estado) {
		this.art_estado = art_estado;
	}
	
	
	
	

}
